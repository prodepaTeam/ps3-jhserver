import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { StoredFile } from './stored-file.model';
import { StoredFilePopupService } from './stored-file-popup.service';
import { StoredFileService } from './stored-file.service';

@Component({
    selector: 'jhi-stored-file-delete-dialog',
    templateUrl: './stored-file-delete-dialog.component.html'
})
export class StoredFileDeleteDialogComponent {

    storedFile: StoredFile;

    constructor(
        private storedFileService: StoredFileService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.storedFileService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'storedFileListModification',
                content: 'Deleted an storedFile'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-stored-file-delete-popup',
    template: ''
})
export class StoredFileDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private storedFilePopupService: StoredFilePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.storedFilePopupService
                .open(StoredFileDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
