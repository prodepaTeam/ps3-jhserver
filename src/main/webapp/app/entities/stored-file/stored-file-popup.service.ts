import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { StoredFile } from './stored-file.model';
import { StoredFileService } from './stored-file.service';

@Injectable()
export class StoredFilePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private storedFileService: StoredFileService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.storedFileService.find(id)
                    .subscribe((storedFileResponse: HttpResponse<StoredFile>) => {
                        const storedFile: StoredFile = storedFileResponse.body;
                        storedFile.createdAt = this.datePipe
                            .transform(storedFile.createdAt, 'yyyy-MM-ddTHH:mm:ss');
                        storedFile.lastModified = this.datePipe
                            .transform(storedFile.lastModified, 'yyyy-MM-ddTHH:mm:ss');
                        this.ngbModalRef = this.storedFileModalRef(component, storedFile);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.storedFileModalRef(component, new StoredFile());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    storedFileModalRef(component: Component, storedFile: StoredFile): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.storedFile = storedFile;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
