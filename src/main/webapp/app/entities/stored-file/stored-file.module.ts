import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Ps3SharedModule } from '../../shared';
import {
    StoredFileService,
    StoredFilePopupService,
    StoredFileComponent,
    StoredFileDetailComponent,
    StoredFileDialogComponent,
    StoredFilePopupComponent,
    StoredFileDeletePopupComponent,
    StoredFileDeleteDialogComponent,
    storedFileRoute,
    storedFilePopupRoute,
    StoredFileResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...storedFileRoute,
    ...storedFilePopupRoute,
];

@NgModule({
    imports: [
        Ps3SharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        StoredFileComponent,
        StoredFileDetailComponent,
        StoredFileDialogComponent,
        StoredFileDeleteDialogComponent,
        StoredFilePopupComponent,
        StoredFileDeletePopupComponent,
    ],
    entryComponents: [
        StoredFileComponent,
        StoredFileDialogComponent,
        StoredFilePopupComponent,
        StoredFileDeleteDialogComponent,
        StoredFileDeletePopupComponent,
    ],
    providers: [
        StoredFileService,
        StoredFilePopupService,
        StoredFileResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Ps3StoredFileModule {}
