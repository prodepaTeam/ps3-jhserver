import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { StoredFile } from './stored-file.model';
import { createRequestOption } from '../../shared';
import { HttpRequest } from '@angular/common/http';

export type EntityResponseType = HttpResponse<StoredFile>;

@Injectable()
export class StoredFileService {

    private resourceUrl =  SERVER_API_URL + 'api/stored-files';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/stored-files';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(storedFile: StoredFile, file: File): Observable<EntityResponseType> {
        const copy = this.convert(storedFile);

        const formdata: FormData = new FormData();
        formdata.append('uploadData', file);
        formdata.append('formData', new Blob([JSON.stringify(copy)], { type: 'application/json' }));

        const req = new HttpRequest('POST', this.resourceUrl, formdata, {
          reportProgress: true,
          responseType: 'text'
        });

        // return this.http.request(req).map((res: EntityResponseType) => this.convertResponse(res));

        // return this.http.post<StoredFile>(this.resourceUploadUrl, copy, { observe: 'response' })
        //       .map((res: EntityResponseType) => this.convertResponse(res));

      return this.http.post<StoredFile>(this.resourceUrl, formdata, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(storedFile: StoredFile): Observable<EntityResponseType> {
        const copy = this.convert(storedFile);
        return this.http.put<StoredFile>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http.get<StoredFile>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<StoredFile[]>> {
        const options = createRequestOption(req);
        return this.http.get<StoredFile[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<StoredFile[]>) => this.convertArrayResponse(res));
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<StoredFile[]>> {
        const options = createRequestOption(req);
        return this.http.get<StoredFile[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<StoredFile[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: StoredFile = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<StoredFile[]>): HttpResponse<StoredFile[]> {
        const jsonResponse: StoredFile[] = res.body;
        const body: StoredFile[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to StoredFile.
     */
    private convertItemFromServer(storedFile: StoredFile): StoredFile {
        const copy: StoredFile = Object.assign({}, storedFile);
        copy.createdAt = this.dateUtils
            .convertDateTimeFromServer(storedFile.createdAt);
        copy.lastModified = this.dateUtils
            .convertDateTimeFromServer(storedFile.lastModified);
        return copy;
    }

    /**
     * Convert a StoredFile to a JSON which can be sent to the server.
     */
    private convert(storedFile: StoredFile): StoredFile {
        const copy: StoredFile = Object.assign({}, storedFile);

        copy.createdAt = this.dateUtils.toDate(storedFile.createdAt);

        copy.lastModified = this.dateUtils.toDate(storedFile.lastModified);
        return copy;
    }
}
