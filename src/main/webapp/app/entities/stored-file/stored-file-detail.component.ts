import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { StoredFile } from './stored-file.model';
import { StoredFileService } from './stored-file.service';

@Component({
    selector: 'jhi-stored-file-detail',
    templateUrl: './stored-file-detail.component.html'
})
export class StoredFileDetailComponent implements OnInit, OnDestroy {

    storedFile: StoredFile;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private storedFileService: StoredFileService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInStoredFiles();
    }

    load(id) {
        this.storedFileService.find(id)
            .subscribe((storedFileResponse: HttpResponse<StoredFile>) => {
                this.storedFile = storedFileResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInStoredFiles() {
        this.eventSubscriber = this.eventManager.subscribe(
            'storedFileListModification',
            (response) => this.load(this.storedFile.id)
        );
    }
}
