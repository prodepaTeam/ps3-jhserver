import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { StoredFile } from './stored-file.model';
import { StoredFilePopupService } from './stored-file-popup.service';
import { StoredFileService } from './stored-file.service';
import { log } from 'util';
import { Application, ApplicationService } from '../application';
import { BucketService, Bucket } from '../bucket';

@Component({
    selector: 'jhi-stored-file-dialog',
    templateUrl: './stored-file-dialog.component.html',
    styleUrls: [
        'stored-file-dialog.css'
    ]
})
export class StoredFileDialogComponent implements OnInit {

    storedFile: StoredFile;
    isSaving: boolean;

    applications: Application[];
    buckets: Bucket[];

    // FileUpload
    currentFileUpload: File;
    selectedFiles: FileList;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private storedFileService: StoredFileService,
        private eventManager: JhiEventManager,
        private applicationService: ApplicationService,
        private bucketService: BucketService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;

        // Load Application Combo
        this.applicationService
            .query({filter: 'application-is-null'}) // O que será isso ? (application-is-null)
            .subscribe((res: HttpResponse<Application[]>) => {       
                
                if (!this.storedFile.ownerApplicationId) {
                    this.applications = res.body;
                } else {
                    this.applicationService
                        .find(this.storedFile.ownerApplicationId)
                        .subscribe((subRes: HttpResponse<Application>) => {
                            this.applications = [subRes.body].concat(res.body);
                        }, (subRes: HttpErrorResponse) => this.onError(subRes.message));
                }
            }, (res: HttpErrorResponse) => this.onError(res.message));


        // Load Application Combo
        /*
        this.bucketService
            .query({filter: 'bucket-is-null'}) // O que será isso ? (application-is-null)
            .subscribe((res: HttpResponse<Application[]>) => {       
                
                if (!this.storedFile.ownerBucketId) {
                    this.buckets = res.body;
                } else {
                    this.bucketService
                        .find(this.storedFile.ownerBucketId)
                        .subscribe((subRes: HttpResponse<Application>) => {
                            this.buckets = [subRes.body].concat(res.body);
                        }, (subRes: HttpErrorResponse) => this.onError(subRes.message));
                }
            }, (res: HttpErrorResponse) => this.onError(res.message));
        */   
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;

        if (this.selectedFiles) {
          this.currentFileUpload = this.selectedFiles.item(0);
        }

        if (this.storedFile.id !== undefined) {
              this.subscribeToSaveResponse(
                this.storedFileService.update(this.storedFile));
        } else {
            this.subscribeToSaveResponse(
                this.storedFileService.create(this.storedFile, this.currentFileUpload));
        }
    }

    selectFile(event) {
        this.selectedFiles = event.target.files;
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<StoredFile>>) {
        result.subscribe((res: HttpResponse<StoredFile>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: StoredFile) {
        this.eventManager.broadcast({ name: 'storedFileListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);

        this.selectedFiles = undefined;
    }

    onChangeApplication() {
        if(this.storedFile.ownerApplicationId) {
            console.log('Carregar Buckets para ' + this.storedFile.ownerApplicationId);

            // TODO Fazer isso
        }
    } 

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackApplicationById(index: number, item: Application) {
        return item.id;
    }

    trackBucketById(index: number, item: Bucket) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-stored-file-popup',
    template: ''
})
export class StoredFilePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private storedFilePopupService: StoredFilePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.storedFilePopupService
                    .open(StoredFileDialogComponent as Component, params['id']);
            } else {
                this.storedFilePopupService
                    .open(StoredFileDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
