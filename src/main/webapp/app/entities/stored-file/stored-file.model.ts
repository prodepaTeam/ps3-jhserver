import { BaseEntity } from './../../shared';

export class StoredFile implements BaseEntity {
    constructor(
        public id?: string,
        public fileName?: string,
        public fileLength?: number,
        public contentType?: string,
        public ownerApplicationId?: string,
        public ownerApplication?: string,
        public ownerBucketId?: string,
        public ownerBucket?: string,
        public createdAt?: any,
        public lastModified?: any,
        public createdBy?: string,
        public lastModifiedBy?: string,
        public uploadedFile?: string,
    ) {
    }
}
