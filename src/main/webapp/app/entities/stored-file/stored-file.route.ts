import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { StoredFileComponent } from './stored-file.component';
import { StoredFileDetailComponent } from './stored-file-detail.component';
import { StoredFilePopupComponent } from './stored-file-dialog.component';
import { StoredFileDeletePopupComponent } from './stored-file-delete-dialog.component';

@Injectable()
export class StoredFileResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const storedFileRoute: Routes = [
    {
        path: 'stored-file',
        component: StoredFileComponent,
        resolve: {
            'pagingParams': StoredFileResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ps3App.storedFile.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'stored-file/:id',
        component: StoredFileDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ps3App.storedFile.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const storedFilePopupRoute: Routes = [
    {
        path: 'stored-file-new',
        component: StoredFilePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ps3App.storedFile.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'stored-file/:id/edit',
        component: StoredFilePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ps3App.storedFile.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'stored-file/:id/delete',
        component: StoredFileDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ps3App.storedFile.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
