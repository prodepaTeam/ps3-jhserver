import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { Ps3ApplicationModule } from './application/application.module';
import { Ps3BucketModule } from './bucket/bucket.module';
import { Ps3StoredFileModule } from './stored-file/stored-file.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        Ps3ApplicationModule,
        Ps3BucketModule,
        Ps3StoredFileModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Ps3EntityModule {}
