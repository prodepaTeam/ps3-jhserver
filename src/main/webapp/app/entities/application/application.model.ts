import { BaseEntity } from './../../shared';

export const enum ApplicationStatus {
    'ACTIVE',
    'INACTIVE'
}

export class Application implements BaseEntity {
    constructor(
        public id?: string,
        public name?: string,
        public status?: ApplicationStatus,
        public createdAt?: any,
        public lastModified?: any,
        public createdBy?: string,
        public lastModifiedBy?: string,
    ) {
    }
}
