import { BaseEntity } from './../../shared';

export class ApplicationRef implements BaseEntity {
    constructor(
        public id?: string,
        public name?: string
    ) {
    }
}
