import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Bucket } from './bucket.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Bucket>;

@Injectable()
export class BucketService {

    private resourceUrl =  SERVER_API_URL + 'api/buckets';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/buckets';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(bucket: Bucket): Observable<EntityResponseType> {
        const copy = this.convert(bucket);
        return this.http.post<Bucket>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(bucket: Bucket): Observable<EntityResponseType> {
        const copy = this.convert(bucket);
        return this.http.put<Bucket>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http.get<Bucket>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Bucket[]>> {
        const options = createRequestOption(req);
        return this.http.get<Bucket[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Bucket[]>) => this.convertArrayResponse(res));
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<Bucket[]>> {
        const options = createRequestOption(req);
        return this.http.get<Bucket[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Bucket[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Bucket = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Bucket[]>): HttpResponse<Bucket[]> {
        const jsonResponse: Bucket[] = res.body;
        const body: Bucket[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Bucket.
     */
    private convertItemFromServer(bucket: Bucket): Bucket {
        const copy: Bucket = Object.assign({}, bucket);
        copy.createdAt = this.dateUtils
            .convertDateTimeFromServer(bucket.createdAt);
        copy.lastModified = this.dateUtils
            .convertDateTimeFromServer(bucket.lastModified);
        return copy;
    }

    /**
     * Convert a Bucket to a JSON which can be sent to the server.
     */
    private convert(bucket: Bucket): Bucket {
        const copy: Bucket = Object.assign({}, bucket);

        copy.createdAt = this.dateUtils.toDate(bucket.createdAt);

        copy.lastModified = this.dateUtils.toDate(bucket.lastModified);
        return copy;
    }
}
