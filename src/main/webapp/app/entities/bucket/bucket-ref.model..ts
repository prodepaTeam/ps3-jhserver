import { BaseEntity } from './../../shared';

export class BucketRef implements BaseEntity {
    constructor(
        public id?: string,
        public name?: string
    ) {
    }
}
