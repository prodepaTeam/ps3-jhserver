import { Application } from '../application/application.model';
import { ApplicationService } from '../application/application.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Bucket } from './bucket.model';
import { BucketPopupService } from './bucket-popup.service';
import { BucketService } from './bucket.service';

@Component({
    selector: 'jhi-bucket-dialog',
    templateUrl: './bucket-dialog.component.html'
})
export class BucketDialogComponent implements OnInit {

    bucket: Bucket;
    isSaving: boolean;
    applications: Application[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private bucketService: BucketService,
        private applicationService: ApplicationService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;

        this.applicationService
            .query({filter: 'application-is-null'})
            .subscribe((res: HttpResponse<Application[]>) => {

                if (!this.bucket.ownerApplicationId) {
                    this.applications = res.body;
                } else {
                    this.applicationService
                        .find(this.bucket.ownerApplicationId)
                        .subscribe((subRes: HttpResponse<Application>) => {
                            this.applications = [subRes.body].concat(res.body);
                        }, (subRes: HttpErrorResponse) => this.onError(subRes.message));
                }
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.bucket.id !== undefined) {
            this.subscribeToSaveResponse(
                this.bucketService.update(this.bucket));
        } else {
            this.subscribeToSaveResponse(
                this.bucketService.create(this.bucket));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Bucket>>) {
        result.subscribe((res: HttpResponse<Bucket>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Bucket) {
        this.eventManager.broadcast({ name: 'bucketListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackApplicationById(index: number, item: Application) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-bucket-popup',
    template: ''
})
export class BucketPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private bucketPopupService: BucketPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.bucketPopupService
                    .open(BucketDialogComponent as Component, params['id']);
            } else {
                this.bucketPopupService
                    .open(BucketDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
