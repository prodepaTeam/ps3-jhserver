import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { Bucket } from './bucket.model';
import { BucketService } from './bucket.service';

@Injectable()
export class BucketPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private bucketService: BucketService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.bucketService.find(id)
                    .subscribe((bucketResponse: HttpResponse<Bucket>) => {
                        const bucket: Bucket = bucketResponse.body;
                        bucket.createdAt = this.datePipe
                            .transform(bucket.createdAt, 'yyyy-MM-ddTHH:mm:ss');
                        bucket.lastModified = this.datePipe
                            .transform(bucket.lastModified, 'yyyy-MM-ddTHH:mm:ss');
                        this.ngbModalRef = this.bucketModalRef(component, bucket);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.bucketModalRef(component, new Bucket());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    bucketModalRef(component: Component, bucket: Bucket): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.bucket = bucket;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
