import { BaseEntity } from './../../shared';
import { ApplicationRef } from '../application/application-ref.model.';

export const enum AccessControl {
    'PUBLIC',
    'USER',
    'APPLICATION'
}

export class Bucket implements BaseEntity {
    constructor(
        public id?: string,
        public name?: string,
        public ownerApplicationId?: string,
        public ownerApplication?: string,
        public accessControl?: AccessControl,
        public createdAt?: any,
        public lastModified?: any,
        public createdBy?: string,
        public lastModifiedBy?: string,
    ) {
    }
}
