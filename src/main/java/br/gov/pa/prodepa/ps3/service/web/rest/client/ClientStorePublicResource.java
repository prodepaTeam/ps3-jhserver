package br.gov.pa.prodepa.ps3.service.web.rest.client;

import com.codahale.metrics.annotation.Timed;
import br.gov.pa.prodepa.ps3.service.service.BucketService;
import br.gov.pa.prodepa.ps3.service.service.StoredFileService;
import br.gov.pa.prodepa.ps3.service.web.rest.errors.BadRequestAlertException;
import br.gov.pa.prodepa.ps3.service.web.rest.util.HeaderUtil;
import br.gov.pa.prodepa.ps3.service.web.rest.util.PaginationUtil;
import br.gov.pa.prodepa.ps3.service.service.dto.BucketDTO;
import br.gov.pa.prodepa.ps3.service.service.dto.StoredFileCreateDTO;
import br.gov.pa.prodepa.ps3.service.service.dto.StoredFileDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Bucket.
 */
@RestController
@RequestMapping("/client/api")
public class ClientStorePublicResource {

    private final Logger log = LoggerFactory.getLogger(ClientStorePublicResource.class);

    private static final String ENTITY_NAME = "storedFile";

    private final BucketService bucketService;
    private final StoredFileService storedFileService;

    public ClientStorePublicResource(BucketService bucketService, StoredFileService storedFileService) {
        this.bucketService = bucketService;
        this.storedFileService = storedFileService;
    }

    /**
     * GET  /stored-files/:id : get the "id" storedFile.
     *
     * @param id the id of the storedFileDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the storedFileDTO, or with status 404 (Not Found)
     */
    @GetMapping("/stored-files/public/{id}")
    @Timed
    public ResponseEntity<StoredFileDTO> getStoredFile(@PathVariable String id) {
        log.debug("REST request to get StoredFile : {}", id);
        
        StoredFileDTO storedFileDTO = storedFileService.findOne(id);
        
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(storedFileDTO));
    }
    
    /**
     * GET  /stored-files/:id : get the "id" storedFile.
     *
     * @param id the id of the storedFileDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the storedFileDTO, or with status 404 (Not Found)
     */
    @GetMapping("/stored-files/public/{id}/data")
    @Timed
    public ResponseEntity<InputStreamResource> getStoredFileData(@PathVariable String id, @RequestParam(required = false, value = "attachmentOption", defaultValue = "false") String attachmentOption) {
        log.debug("REST request to get StoredFile : {}", id);
        return storedFileService.findOneData(id, attachmentOption);
        //return ResponseUtil.wrapOrNotFound(Optional.ofNullable(resp));
    }
        
    /**
     * GET  /buckets : get all the buckets.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of buckets in body
     */
    @GetMapping("/ping/stored-files/public")
    //@Timed
    public ResponseEntity<String> ping() {
        log.debug("REST request to get a page of Buckets");
        return new ResponseEntity<>("{'msg':'OK'}", HttpStatus.OK);
    }

}
