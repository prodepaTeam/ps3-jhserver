package br.gov.pa.prodepa.ps3.service.web.rest.client;

import com.codahale.metrics.annotation.Timed;
import br.gov.pa.prodepa.ps3.service.service.BucketService;
import br.gov.pa.prodepa.ps3.service.service.StoredFileService;
import br.gov.pa.prodepa.ps3.service.web.rest.errors.BadRequestAlertException;
import br.gov.pa.prodepa.ps3.service.web.rest.util.HeaderUtil;
import br.gov.pa.prodepa.ps3.service.web.rest.util.PaginationUtil;
import br.gov.pa.prodepa.ps3.service.service.dto.BucketDTO;
import br.gov.pa.prodepa.ps3.service.service.dto.StoredFileCreateDTO;
import br.gov.pa.prodepa.ps3.service.service.dto.StoredFileDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Bucket.
 */
@RestController
@RequestMapping("/client/api")
public class ClientStoreResource {

    private final Logger log = LoggerFactory.getLogger(ClientStoreResource.class);

    private static final String ENTITY_NAME = "storedFile";

    private final BucketService bucketService;
    private final StoredFileService storedFileService;

    public ClientStoreResource(BucketService bucketService, StoredFileService storedFileService) {
        this.bucketService = bucketService;
        this.storedFileService = storedFileService;
    }

    /**
     * POST  /buckets : Create a new bucket.
     *
     * @param bucketDTO the bucketDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new bucketDTO, or with status 400 (Bad Request) if the bucket has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/stored-files")
    @Timed
    public ResponseEntity<StoredFileDTO> storeFile(@RequestParam(name = "applicationId", defaultValue = "eProtocolo") String applicationId, 
    											   @RequestParam("bucketId") String bucketId, 
    											   @RequestParam("file") MultipartFile file, 
    											   @RequestHeader HttpHeaders headers) throws URISyntaxException {
    	
    	if (file == null) {
    		throw new BadRequestAlertException("The MultipartFile can not be identified", ENTITY_NAME, "filenotfound");
    	}

    	log.debug("REST request to save Bucket : {}", file.getOriginalFilename());
    	
    	StoredFileDTO storedFileDTO = new StoredFileDTO();
    	storedFileDTO.setOwnerBucket(bucketId);
    	storedFileDTO.setOwnerApplication(applicationId); // TODO O identificador da aplicacao vira do token, não do parametro
    	
		storedFileDTO = storedFileService.save(storedFileDTO, file);
    	
    	return ResponseEntity.created(new URI("/api/stored-files/" + storedFileDTO.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, storedFileDTO.getId().toString()))
                .body(storedFileDTO);
    }

    

    /**
     * 
     * WARN: não háverá update aqui. O que poderá haver é upload de nova versão
     * 
     * PUT  /stored-files : Updates an existing storedFile.
     *
     * @param storedFileDTO the storedFileDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated storedFileDTO,
     * or with status 400 (Bad Request) if the storedFileDTO is not valid,
     * or with status 500 (Internal Server Error) if the storedFileDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    /*@PutMapping("/client-api/stored-files")
    @Timed
    public ResponseEntity<StoredFileDTO> updateStoredFile(@RequestBody StoredFileDTO storedFileDTO) throws URISyntaxException {
        log.debug("REST request to update StoredFile : {}", storedFileDTO);
        if (storedFileDTO.getId() == null) {
            return createStoredFile(storedFileDTO);
        }
        StoredFileDTO result = storedFileService.save(storedFileDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, storedFileDTO.getId().toString()))
            .body(result);
    }*/

    /**
     * GET  /stored-files : get all the storedFiles.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of storedFiles in body
     */
    @GetMapping("/stored-files")
    @Timed
    public ResponseEntity<List<StoredFileDTO>> getAllStoredFiles(Pageable pageable) {
        log.debug("REST request to get a page of StoredFiles");
        Page<StoredFileDTO> page = storedFileService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/stored-files");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /stored-files/:id : get the "id" storedFile.
     *
     * @param id the id of the storedFileDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the storedFileDTO, or with status 404 (Not Found)
     */
    @GetMapping("/stored-files/{id}")
    @Timed
    public ResponseEntity<StoredFileDTO> getStoredFile(@PathVariable String id) {
        log.debug("REST request to get StoredFile : {}", id);
        
        StoredFileDTO storedFileDTO = storedFileService.findOne(id);
        
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(storedFileDTO));
    }
    
    
    /**
     * GET  /stored-files/:id : get the "id" storedFile.
     *
     * @param id the id of the storedFileDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the storedFileDTO, or with status 404 (Not Found)
     */
    @GetMapping("/stored-files/{id}/data")
    @Timed
    public ResponseEntity<InputStreamResource> getStoredFileData(@PathVariable String id, @RequestParam(required = false, value = "attachmentOption", defaultValue = "false") String attachmentOption) {
        log.debug("REST request to get StoredFile : {}", id);
        return storedFileService.findOneData(id, attachmentOption);
        //return ResponseUtil.wrapOrNotFound(Optional.ofNullable(resp));
    }

    /**
     * DELETE  /stored-files/:id : delete the "id" storedFile.
     *
     * @param id the id of the storedFileDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/stored-files/{id}")
    @Timed
    public ResponseEntity<Void> deleteStoredFile(@PathVariable String id) {
        log.debug("REST request to delete StoredFile : {}", id);
        storedFileService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * SEARCH  /_search/stored-files?query=:query : search for the storedFile corresponding
     * to the query.
     *
     * @param query the query of the storedFile search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/stored-files")
    @Timed
    public ResponseEntity<List<StoredFileDTO>> searchStoredFiles(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of StoredFiles for query {}", query);
        Page<StoredFileDTO> page = storedFileService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/stored-files");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }    
    
    /**
     * GET  /buckets : get all the buckets.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of buckets in body
     */
    @GetMapping("/ping/stored-files")
    //@Timed
    public ResponseEntity<String> ping() {
        log.debug("REST request to get a page of Buckets");
        return new ResponseEntity<>("{'msg':'OK'}", HttpStatus.OK);
    }

}
