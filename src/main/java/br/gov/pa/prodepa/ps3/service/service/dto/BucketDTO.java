package br.gov.pa.prodepa.ps3.service.service.dto;


import java.time.Instant;
import java.io.Serializable;
import java.util.Objects;
import br.gov.pa.prodepa.ps3.service.domain.enumeration.AccessControl;
import br.gov.pa.prodepa.ps3.service.domain.refs.ApplicationRef;

/**
 * A DTO for the Bucket entity.
 */
public class BucketDTO extends AbstractS3DTO {

	private static final long serialVersionUID = 3509434197252255918L;

	private String name;

    //private ApplicationRef ownerApplication;
    private String ownerApplicationId;
    private String ownerApplication;

    private AccessControl accessControl;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwnerApplicationId() {
        return ownerApplicationId;
    }

    public void setOwnerApplicationId(String ownerApplicationId) {
        this.ownerApplicationId = ownerApplicationId;
    }

    public String getOwnerApplication() {
        return ownerApplication;
    }

    public void setOwnerApplication(String ownerApplication) {
        this.ownerApplication = ownerApplication;
    }

    public AccessControl getAccessControl() {
        return accessControl;
    }

    public void setAccessControl(AccessControl accessControl) {
        this.accessControl = accessControl;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BucketDTO bucketDTO = (BucketDTO) o;
        if(bucketDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bucketDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BucketDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", ownerApplication=" + getOwnerApplication() +
            ", accessControl='" + getAccessControl() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", lastModified='" + getLastModified() + "'" +
            ", createdBy=" + getCreatedBy() +
            ", lastModifiedBy=" + getLastModifiedBy() +
            "}";
    }
}
