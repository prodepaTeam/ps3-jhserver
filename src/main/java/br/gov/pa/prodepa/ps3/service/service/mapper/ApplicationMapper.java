package br.gov.pa.prodepa.ps3.service.service.mapper;

import br.gov.pa.prodepa.ps3.service.domain.*;
import br.gov.pa.prodepa.ps3.service.service.dto.ApplicationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Application and its DTO ApplicationDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ApplicationMapper extends EntityMapper<ApplicationDTO, Application> {


}
