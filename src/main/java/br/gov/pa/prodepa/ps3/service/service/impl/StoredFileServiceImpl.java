package br.gov.pa.prodepa.ps3.service.service.impl;

import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsCriteria;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSFile;

import br.gov.pa.prodepa.ps3.service.domain.Application;
import br.gov.pa.prodepa.ps3.service.domain.Bucket;
import br.gov.pa.prodepa.ps3.service.domain.StoredFile;
import br.gov.pa.prodepa.ps3.service.domain.refs.ApplicationRef;
import br.gov.pa.prodepa.ps3.service.domain.refs.BucketRef;
import br.gov.pa.prodepa.ps3.service.repository.ApplicationRepository;
import br.gov.pa.prodepa.ps3.service.repository.BucketRepository;
import br.gov.pa.prodepa.ps3.service.repository.StoredFileRepository;
import br.gov.pa.prodepa.ps3.service.repository.search.StoredFileSearchRepository;
import br.gov.pa.prodepa.ps3.service.security.jwt.TokenProvider;
import br.gov.pa.prodepa.ps3.service.service.AbstractS3Service;
import br.gov.pa.prodepa.ps3.service.service.PdfTextExtractorService;
import br.gov.pa.prodepa.ps3.service.service.StoredFileService;
import br.gov.pa.prodepa.ps3.service.service.dto.StoredFileDTO;
import br.gov.pa.prodepa.ps3.service.service.mapper.StoredFileMapper;
import br.gov.pa.prodepa.ps3.service.web.rest.errors.APIException;
import br.gov.pa.prodepa.ps3.service.web.rest.errors.BadRequestAlertException;

/**
 * Service Implementation for managing StoredFile.
 */
@Service
public class StoredFileServiceImpl extends AbstractS3Service implements StoredFileService {

    private final Logger log = LoggerFactory.getLogger(StoredFileServiceImpl.class);

    private final StoredFileRepository storedFileRepository;

    private final StoredFileMapper storedFileMapper;

    private final StoredFileSearchRepository storedFileSearchRepository;
    
    private final GridFsTemplate gridFsTemplate;
    
    private final ApplicationRepository applicationRepository;
    private final BucketRepository bucketRepository;
    
    private TokenProvider tokenProvider;
    
    
    private PdfTextExtractorService pdfTextExtractor;
    
    public StoredFileServiceImpl(StoredFileRepository storedFileRepository, 
    							 StoredFileMapper storedFileMapper, 
    							 StoredFileSearchRepository storedFileSearchRepository, 
    							 GridFsTemplate gridFsTemplate,
    							 ApplicationRepository applicationRepository,
    							 BucketRepository bucketRepository,
    							 TokenProvider tokenProvider,
    							 PdfTextExtractorService pdfTextExtractor) {
        this.storedFileRepository = storedFileRepository;
        this.storedFileMapper = storedFileMapper;
        this.storedFileSearchRepository = storedFileSearchRepository;
        this.gridFsTemplate = gridFsTemplate;
        this.applicationRepository = applicationRepository;
        this.bucketRepository = bucketRepository;
        this.tokenProvider = tokenProvider;
        this.pdfTextExtractor = pdfTextExtractor;
    }

    /**
     * Save a storedFile.
     *
     * @param storedFileDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public StoredFileDTO save(StoredFileDTO storedFileDTO, MultipartFile file) {
        log.debug("Request to save StoredFile : {}", storedFileDTO);
        
        /*
         * Validation 
         * TODO Move to specific method
         * 
         */
        Application application = null;
        Bucket bucket = null;
        // TODO Obter identificador a aplicacao. Obter do Token, se APP; ou do parametro, se web
        if(tokenProvider.checkClaim(TokenProvider.TOKEN_TYPE_KEY, TokenProvider.TOKEN_TYPE_APPLICATION)) {
        	
        	String ownerApplicationId = tokenProvider.getClaims(TokenProvider.APPLICATION_ID_KEY);
        	application = applicationRepository.findOne(ownerApplicationId);
        	
        	if(storedFileDTO.getOwnerBucketId() != null) {
        		bucket = bucketRepository.findOne(storedFileDTO.getOwnerBucketId());
        	} else {
        		bucket = bucketRepository.findOneByName(storedFileDTO.getOwnerBucket());
        	}
        	
        	
        } else {
        	
        	if(isEmpty(storedFileDTO.getOwnerApplicationId())) {
            	throw new BadRequestAlertException("The Application identifier is required", "storedFiles", "invalidApplication");
            }
        	
        	application = applicationRepository.findOne(storedFileDTO.getOwnerApplicationId());
        	
        	if(isEmpty(storedFileDTO.getOwnerBucketId())) {
            	throw new BadRequestAlertException("The Bucket identifier is required", "storedFiles", "invalidBucket");
            }
        	
        	bucket = bucketRepository.findOne(storedFileDTO.getOwnerBucketId());
        	
        }
        
        //Validate Application
        if(application == null) {
        	throw new BadRequestAlertException("The "+storedFileDTO.getOwnerApplication()+" application is invalid", "storedFiles", "invalidApplication");
        }
        
        //Validate Bucket
        if(bucket == null) {
        	throw new BadRequestAlertException("The "+storedFileDTO.getOwnerBucket()+" bucket is invalid", "storedFiles", "invalidApplication");
        }
        
        //Get the File uploaded info
        storedFileDTO.setFileName(file.getOriginalFilename());
    	storedFileDTO.setContentType(file.getContentType());
    	storedFileDTO.setFileLength(file.getSize() / 1024);

        StoredFile storedFile = storedFileMapper.toEntity(storedFileDTO);
        
        String objectFileId = storeFileInGridFS(storedFileDTO, file);
        storedFile.setFileId(objectFileId);
        
        //Não ficou a cargo do Mapper, pq eu precido de mais info que o dto tem.
        storedFile.setOwnerBucket(new BucketRef(bucket));
        storedFile.setOwnerApplication(new ApplicationRef(application));
        
        prepareInsert(storedFile);
        
        storedFile = storedFileRepository.save(storedFile);
        
        
        if (file.getContentType().equals("application/pdf")) {
        	try {
				//asyncTextExtraction(storedFile.getId(), file.getBytes());
				pdfTextExtractor.asyncTextExtraction(storedFile.getId(), file.getBytes());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        } else if (file.getContentType().equals(MediaType.TEXT_PLAIN_VALUE)
        		|| file.getContentType().equals(MediaType.TEXT_MARKDOWN_VALUE)
        		|| file.getContentType().equals(MediaType.TEXT_HTML_VALUE)
        		|| file.getContentType().equals(MediaType.TEXT_XML_VALUE)
        		) {
        	try {
        		storedFile.setContent(new String(file.getBytes()));
        	} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        
        StoredFileDTO result = storedFileMapper.toDto(storedFile);
        storedFileSearchRepository.save(storedFile);
        return result;
    }
    
	private String storeFileInGridFS(StoredFileDTO storedFileDTO, MultipartFile file) {

		String name = file.getOriginalFilename();
		String contentType = file.getContentType();

		Optional<GridFSDBFile> existing = checkIfExists(name);
		if (existing.isPresent()) {
			//TODO A abordagem será de versionamento
			gridFsTemplate.delete(getFilenameQuery(name));
		}

		try {
			GridFSFile gridFSFile;
			gridFSFile = gridFsTemplate.store(file.getInputStream(), name, contentType);
			org.bson.types.ObjectId objectFileId = (org.bson.types.ObjectId) gridFSFile.getId();
			
			gridFSFile.save();
			
			return objectFileId.toHexString(); 
		} catch (IOException e) {
			throw new BadRequestAlertException("The file can't be stored:"+e.getMessage(), "storedFiles", "invalidApplication");
		}
		

	}

    /**
     * Get all the storedFiles.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    public Page<StoredFileDTO> findAll(Pageable pageable) {
        log.debug("Request to get all StoredFiles");
        return storedFileRepository.findAll(pageable)
            .map(storedFileMapper::toDto);
    }

    /**
     * Get one storedFile by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public StoredFileDTO findOne(String id) {
        log.debug("Request to get StoredFile : {}", id);
        StoredFile storedFile = storedFileRepository.findOne(id);
        return storedFileMapper.toDto(storedFile);
    }

    /**
     * Get one storedFile by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public ResponseEntity<InputStreamResource> findOneData(String id, String attachmentOption) {
        log.debug("Request to get StoredFile : {}", id);
        
        StoredFile storedFile = storedFileRepository.findOne(id);
        if(storedFile == null) {
        	//return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        	throw new APIException("storedFiles", "fileNotFound", "The file "+id+" was not found.");
        }
        
        return retrieveGridFsFile(storedFile.getFileId(), attachmentOption);
    }

	private ResponseEntity<InputStreamResource> retrieveGridFsFile(String objectFileId, String attachmentOption) {
		try {

			Query idQuery = Query.query(GridFsCriteria.where("_id").is(objectFileId));

			GridFSDBFile file = gridFsTemplate.findOne(idQuery);
			Optional<GridFSDBFile> optionalCreated = Optional.ofNullable(file);

			if (optionalCreated.isPresent()) {
				GridFSDBFile created = optionalCreated.get();
				ByteArrayOutputStream os = new ByteArrayOutputStream();
				created.writeTo(os);

				HttpHeaders headers = new HttpHeaders();
				headers.add(HttpHeaders.CONTENT_TYPE, created.getContentType());
				
				if(attachmentOption != null && Boolean.parseBoolean(attachmentOption)) {
					headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename='" + created.getFilename() + "'");
				} else {
					headers.add(HttpHeaders.CONTENT_DISPOSITION, "inline; filename='" + created.getFilename() + "'");
				}
				

				return ResponseEntity.ok()
									 .contentType(MediaType.valueOf(created.getContentType()))
									 .headers(headers)
									 .body(new InputStreamResource(created.getInputStream()));

			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (IOException e) {
			return new ResponseEntity<>(HttpStatus.IM_USED);
		}
	}
    
    /**
     * Delete the storedFile by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete StoredFile : {}", id);
        
        StoredFile storedFile = storedFileRepository.findOne(id);
        
        if(storedFile != null) {
        	gridFsTemplate.delete(getFileIdQuery(storedFile.getFileId()));
        }
        
        storedFileRepository.delete(id);
        storedFileSearchRepository.delete(id);
    }

    /**
     * Search for the storedFile corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    public Page<StoredFileDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of StoredFiles for query {}", query);
        Page<StoredFile> result = storedFileSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(storedFileMapper::toDto);
    }
    
	private List<GridFSDBFile> getFiles() {
		return gridFsTemplate.find(null);
	}

	private Optional<GridFSDBFile> checkIfExists(String id) {
		GridFSDBFile file = gridFsTemplate.findOne(getFileIdQuery(id));
		return Optional.ofNullable(file);
	}

	private static Query getFilenameQuery(String name) {
		return Query.query(GridFsCriteria.whereFilename().is(name));
	}
	
	private static Query getFileIdQuery(String id) {
		return Query.query(GridFsCriteria.where("_id").is(id));
	}
}
