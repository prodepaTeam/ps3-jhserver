package br.gov.pa.prodepa.ps3.service.service.mapper;

import br.gov.pa.prodepa.ps3.service.domain.*;
import br.gov.pa.prodepa.ps3.service.domain.refs.ApplicationRef;
import br.gov.pa.prodepa.ps3.service.domain.refs.BucketRef;
import br.gov.pa.prodepa.ps3.service.service.dto.StoredFileDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity StoredFile and its DTO StoredFileDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface StoredFileMapper extends EntityMapper<StoredFileDTO, StoredFile> {

	@Mappings({
        @Mapping(source = "ownerApplication.name", target = "ownerApplication"),
        @Mapping(source = "ownerBucket.name", target = "ownerBucket")
    })
	StoredFileDTO toDto(StoredFile entity);
	
	
	/*default String applicationRefToString(ApplicationRef obj) {
        return obj.getName();
    }
	
	default String bucketRefToString(BucketRef obj) {
        return obj.getName();
    }*/
	
	default ApplicationRef applicationRefToString(String id) {
        return new ApplicationRef(id);
    }
	
	default BucketRef bucketRefToString(String id) {
        return new BucketRef(id);
    }
}
