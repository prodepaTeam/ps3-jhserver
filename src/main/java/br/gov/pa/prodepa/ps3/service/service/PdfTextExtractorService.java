package br.gov.pa.prodepa.ps3.service.service;

import java.io.InputStream;

import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import br.gov.pa.prodepa.ps3.service.service.dto.StoredFileDTO;

/**
 * Service Interface for managing StoredFile.
 */
public interface PdfTextExtractorService {

    /**
     * Save a storedFile.
     *
     * @param storedFileDTO the entity to save
     * @param bs 
     * @return the persisted entity
     */
	public void asyncTextExtraction(String fileId, byte[] data);
}
