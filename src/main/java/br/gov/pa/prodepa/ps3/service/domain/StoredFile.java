package br.gov.pa.prodepa.ps3.service.domain;

import java.util.Objects;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import br.gov.pa.prodepa.ps3.service.domain.refs.ApplicationRef;
import br.gov.pa.prodepa.ps3.service.domain.refs.BucketRef;

/**
 * A StoredFile.
 */
@Document(collection = "stored_file")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "storedfile")
public class StoredFile extends AbstractS3Entity<StoredFile> {

	private static final long serialVersionUID = -8899013050373861576L;

	@Field("file_id")
    private String fileId;
    
    @Field("file_name")
    private String fileName;

    @Field("content_type")
    private String contentType;
    
    @Field("content")
    private String content;
    
    @Field("file_lenght")
    private Long fileLength;

    @Field("owner_application")
    private ApplicationRef ownerApplication;

    @Field("owner_bucket")
    private BucketRef ownerBucket;


    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove

    public String getFileId() {
		return fileId;
	}

    public StoredFile fileId(String fileId) {
		this.fileId = fileId;
		return this;
	}
    
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public String getFileName() {
        return fileName;
    }

    public StoredFile fileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getContentType() {
        return contentType;
    }

    public StoredFile contentType(String contentType) {
        this.contentType = contentType;
        return this;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public ApplicationRef getOwnerApplication() {
        return ownerApplication;
    }

    public StoredFile ownerApplication(ApplicationRef ownerApplication) {
        this.ownerApplication = ownerApplication;
        return this;
    }

    public void setOwnerApplication(ApplicationRef ownerApplication) {
        this.ownerApplication = ownerApplication;
    }

    public BucketRef getOwnerBucket() {
        return ownerBucket;
    }

    public StoredFile ownerBucket(BucketRef ownerBucket) {
        this.ownerBucket = ownerBucket;
        return this;
    }

    public void setOwnerBucket(BucketRef ownerBucket) {
        this.ownerBucket = ownerBucket;
    }
    
    public Long getFileLength() {
		return fileLength;
	}

	public void setFileLength(Long fileLength) {
		this.fileLength = fileLength;
	}
	
	
	
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	public StoredFile content(String content) {
		this.content = content;
		return this;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StoredFile storedFile = (StoredFile) o;
        if (storedFile.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), storedFile.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "StoredFile{" +
            "id=" + getId() +
            ", fileName='" + getFileName() + "'" +
            ", mimeType='" + getContentType() + "'" +
            ", ownerApplication=" + getOwnerApplication() +
            ", ownerBucket=" + getOwnerBucket() +
            ", createdAt='" + getCreatedAt() + "'" +
            ", lastModified='" + getLastModified() + "'" +
            ", createdBy=" + getCreatedBy() +
            ", lastModifiedBy=" + getLastModifiedBy() +
            "}";
    }
}
