package br.gov.pa.prodepa.ps3.service.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import br.gov.pa.prodepa.ps3.service.domain.enumeration.TokenType;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * An Token used by Spring Security.
 */
public class ApplicationToken implements Serializable {

    private static final long serialVersionUID = 1L;

    private String token;
    
    private TokenType type;
    
    private Boolean active;

    
    public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public TokenType getType() {
		return type;
	}

	public void setType(TokenType type) {
		this.type = type;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ApplicationToken authority = (ApplicationToken) o;

        return !(token != null ? !token.equals(authority.token) : authority.token != null);
    }

    @Override
    public int hashCode() {
        return token != null ? token.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Authority{" +
            "token='" + token + '\'' +
            "}";
    }
}
