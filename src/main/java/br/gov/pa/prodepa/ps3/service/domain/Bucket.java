package br.gov.pa.prodepa.ps3.service.domain;

import java.util.Objects;

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import br.gov.pa.prodepa.ps3.service.domain.enumeration.AccessControl;
import br.gov.pa.prodepa.ps3.service.domain.refs.ApplicationRef;

/**
 * A Bucket.
 */
@Document(collection = "bucket")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "bucket")
public class Bucket extends AbstractS3Entity<Bucket> {

	private static final long serialVersionUID = -351528277488213379L;

	@Field("name")
    private String name;

    @Field("owner_application")
    //@DBRef(lazy = true)
    private ApplicationRef ownerApplication;

    @Field("access_control")
    private AccessControl accessControl;


    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove

    public String getName() {
        return name;
    }

    public Bucket name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ApplicationRef getOwnerApplication() {
        return ownerApplication;
    }

    public Bucket ownerApplication(ApplicationRef ownerApplication) {
        this.ownerApplication = ownerApplication;
        return this;
    }

    public void setOwnerApplication(ApplicationRef ownerApplication) {
        this.ownerApplication = ownerApplication;
    }

    public AccessControl getAccessControl() {
        return accessControl;
    }

    public Bucket accessControl(AccessControl accessControl) {
        this.accessControl = accessControl;
        return this;
    }

    public void setAccessControl(AccessControl accessControl) {
        this.accessControl = accessControl;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Bucket bucket = (Bucket) o;
        if (bucket.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bucket.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Bucket{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", ownerApplication=" + getOwnerApplication() +
            ", accessControl='" + getAccessControl() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", lastModified='" + getLastModified() + "'" +
            ", createdBy=" + getCreatedBy() +
            ", lastModifiedBy=" + getLastModifiedBy() +
            "}";
    }
}
