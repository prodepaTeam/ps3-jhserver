package br.gov.pa.prodepa.ps3.service.service.mapper;

import br.gov.pa.prodepa.ps3.service.domain.*;
import br.gov.pa.prodepa.ps3.service.domain.refs.ApplicationRef;
import br.gov.pa.prodepa.ps3.service.service.dto.BucketDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Bucket and its DTO BucketDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BucketMapper extends EntityMapper<BucketDTO, Bucket> {


    @Mappings({
        @Mapping(source = "ownerApplication.id", target = "ownerApplicationId"),
        @Mapping(source = "ownerApplication.name", target = "ownerApplication")
    })
    BucketDTO toDto(Bucket entity);
    
    default ApplicationRef applicationFromId(String id) {
         if (id == null) {
              return null;
         }
         return new ApplicationRef(id);
    }

    // @Mapping(source = "ownerApplication.id", target = "ownerApplication")
    // BucketDTO toDto(Bucket location);

    // @Mapping(source = "ownerApplication", target = "ownerApplication")
    // Bucket toEntity(BucketDTO locationDTO);

    // default Bucket fromId(String id) {
    //     if (id == null) {
    //         return null;
    //     }
    //     Bucket location = new Bucket();
    //     location.setId(id);
    //     return location;
    // }

}
