package br.gov.pa.prodepa.ps3.service.service.impl;

import static org.apache.commons.lang3.StringUtils.isEmpty;

import br.gov.pa.prodepa.ps3.service.service.AbstractS3Service;
import br.gov.pa.prodepa.ps3.service.service.BucketService;
import br.gov.pa.prodepa.ps3.service.domain.Application;
import br.gov.pa.prodepa.ps3.service.domain.Bucket;
import br.gov.pa.prodepa.ps3.service.domain.User;
import br.gov.pa.prodepa.ps3.service.domain.refs.ApplicationRef;
import br.gov.pa.prodepa.ps3.service.repository.ApplicationRepository;
import br.gov.pa.prodepa.ps3.service.repository.BucketRepository;
import br.gov.pa.prodepa.ps3.service.repository.search.BucketSearchRepository;
import br.gov.pa.prodepa.ps3.service.service.dto.BucketDTO;
import br.gov.pa.prodepa.ps3.service.service.mapper.BucketMapper;
import br.gov.pa.prodepa.ps3.service.web.rest.errors.BadRequestAlertException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Bucket.
 */
@Service
public class BucketServiceImpl extends AbstractS3Service implements BucketService {

    private final Logger log = LoggerFactory.getLogger(BucketServiceImpl.class);

    private final BucketRepository bucketRepository;

    private final BucketMapper bucketMapper;

    private final BucketSearchRepository bucketSearchRepository;
    private final ApplicationRepository applicationRepository;

    public BucketServiceImpl(BucketRepository bucketRepository, BucketMapper bucketMapper, BucketSearchRepository bucketSearchRepository, ApplicationRepository applicationRepository) {
        this.bucketRepository = bucketRepository;
        this.bucketMapper = bucketMapper;
        this.bucketSearchRepository = bucketSearchRepository;
        this.applicationRepository = applicationRepository;
    }

    /**
     * Save a bucket.
     *
     * @param bucketDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public BucketDTO save(BucketDTO bucketDTO) {
        log.debug("Request to save Bucket : {}", bucketDTO);
        
        if(bucketDTO.getOwnerApplicationId() == null) {
        	throw new BadRequestAlertException("The Application identifier is required", "storedFiles", "invalidApplication");
        }
        
        Application application = applicationRepository.findOne(bucketDTO.getOwnerApplicationId());//TODO Talves não venha do cliente o oID em si, mas um outro código mais siginificativo, com EPROTOCOLO
        if(application == null) {
        	throw new BadRequestAlertException("The "+bucketDTO.getOwnerApplicationId()+" application is invalid", "storedFiles", "invalidApplication");
        }
                
        Bucket bucket = bucketMapper.toEntity(bucketDTO);
        
        //Complementar os dados da referencia
        bucket.setOwnerApplication(new ApplicationRef(application));

        log.debug(">>>> {}", bucket.getOwnerApplication());

        prepareInsert(bucket);

        bucket = bucketRepository.save(bucket);
        BucketDTO result = bucketMapper.toDto(bucket);  

        bucketSearchRepository.save(bucket);
        return result;
    }

    /**
     * Get all the buckets.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    public Page<BucketDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Buckets");
        return bucketRepository.findAll(pageable)
            .map(bucketMapper::toDto);
    }

    /**
     * Get one bucket by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public BucketDTO findOne(String id) {
        log.debug("Request to get Bucket : {}", id);
        Bucket bucket = bucketRepository.findOne(id);
        return bucketMapper.toDto(bucket);
    }
    
    

    /**
     * Get one bucket by Name.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public BucketDTO findOneByName(String name) {
    	log.debug("Request to get Bucket : {}", name);
        Bucket bucket = bucketRepository.findOneByName(name);
        
        if(bucket == null) {
    		throw new BadRequestAlertException("The Bucket name is invalid", "bucket", "invalidbucketname");
    	}
        
        return bucketMapper.toDto(bucket);
    }

    /**
     * Delete the bucket by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Bucket : {}", id);
        bucketRepository.delete(id);
        bucketSearchRepository.delete(id);
    }

    /**
     * Search for the bucket corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    public Page<BucketDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Buckets for query {}", query);
        Page<Bucket> result = bucketSearchRepository.search(queryStringQuery(query), pageable);
        return result.map(bucketMapper::toDto);
    }
}
