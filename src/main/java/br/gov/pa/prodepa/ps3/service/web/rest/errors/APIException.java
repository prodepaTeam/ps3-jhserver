package br.gov.pa.prodepa.ps3.service.web.rest.errors;

public class APIException extends BadRequestAlertException {

	private static final long serialVersionUID = 5726231813064556769L;

	public APIException(String message) {
        super(ErrorConstants.DEFAULT_API_TYPE, message, "defaultapi", "defaultapi");
    }
    
    public APIException(String entityName, String errorKey, String message) {
        super(ErrorConstants.DEFAULT_API_TYPE, message, entityName, errorKey);
    }
}
