package br.gov.pa.prodepa.ps3.service.domain.refs;

import java.io.Serializable;
import java.util.Objects;

import br.gov.pa.prodepa.ps3.service.domain.Bucket;

/**
 * A Bucket.
 */
public class BucketRef implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private String name;
    
    public BucketRef() {
		super();
	}

	public BucketRef(String id) {
		super();
		this.id = id;
	}

	public BucketRef(Bucket bucket) {
		super();
		this.id = bucket.getId();
		this.name = bucket.getName();
	}

	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public BucketRef name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BucketRef bucket = (BucketRef) o;
        if (bucket.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bucket.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Bucket{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
