package br.gov.pa.prodepa.ps3.service.domain.enumeration;

/**
 * The AccessControl enumeration.
 */
public enum AccessControl {
    PUBLIC, USER, APPLICATION
}
