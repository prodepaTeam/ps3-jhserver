package br.gov.pa.prodepa.ps3.service.repository;

import br.gov.pa.prodepa.ps3.service.domain.Bucket;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the Bucket entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BucketRepository extends MongoRepository<Bucket, String> {

	
	/**
	 * Retrieves an entity by its Name.
	 * 
	 * @param id must not be {@literal null}.
	 * @return the entity with the given id or {@literal null} if none found
	 * @throws IllegalArgumentException if {@code id} is {@literal null}
	 */
	Bucket findOneByName(String id);
}
