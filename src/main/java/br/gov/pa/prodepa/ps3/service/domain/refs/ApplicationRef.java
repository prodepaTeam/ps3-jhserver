package br.gov.pa.prodepa.ps3.service.domain.refs;

import java.io.Serializable;
import java.util.Objects;

import br.gov.pa.prodepa.ps3.service.domain.Application;

/**
 * A Application.
 */
public class ApplicationRef implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private String name;

    public ApplicationRef() {
		super();
	}

	public ApplicationRef(String id) {
		super();
		this.id = id;
	}

	public ApplicationRef(Application application) {
		super();
		this.id = application.getId();
		this.name = application.getName();
	}

	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public ApplicationRef name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ApplicationRef application = (ApplicationRef) o;
        if (application.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), application.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Application{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
