package br.gov.pa.prodepa.ps3.service.repository.search;

import br.gov.pa.prodepa.ps3.service.domain.Application;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Application entity.
 */
public interface ApplicationSearchRepository extends ElasticsearchRepository<Application, String> {
}
