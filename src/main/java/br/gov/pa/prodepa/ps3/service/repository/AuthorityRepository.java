package br.gov.pa.prodepa.ps3.service.repository;

import br.gov.pa.prodepa.ps3.service.domain.Authority;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the Authority entity.
 */
public interface AuthorityRepository extends MongoRepository<Authority, String> {
}
