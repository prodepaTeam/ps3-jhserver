package br.gov.pa.prodepa.ps3.service.repository.search;

import br.gov.pa.prodepa.ps3.service.domain.Bucket;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Bucket entity.
 */
public interface BucketSearchRepository extends ElasticsearchRepository<Bucket, String> {
}
