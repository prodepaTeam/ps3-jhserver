/**
 * Specific errors used with Zalando's "problem-spring-web" library.
 *
 * More information on https://github.com/zalando/problem-spring-web
 */
package br.gov.pa.prodepa.ps3.service.web.rest.errors;
