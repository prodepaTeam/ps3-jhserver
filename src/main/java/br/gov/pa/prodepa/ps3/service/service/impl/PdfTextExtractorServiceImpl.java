package br.gov.pa.prodepa.ps3.service.service.impl;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;

import br.gov.pa.prodepa.ps3.service.domain.StoredFile;
import br.gov.pa.prodepa.ps3.service.repository.StoredFileRepository;
import br.gov.pa.prodepa.ps3.service.repository.search.StoredFileSearchRepository;
import br.gov.pa.prodepa.ps3.service.service.AbstractS3Service;
import br.gov.pa.prodepa.ps3.service.service.PdfTextExtractorService;

/**
 * Service Implementation for managing StoredFile.
 */
@Service
public class PdfTextExtractorServiceImpl extends AbstractS3Service implements PdfTextExtractorService {

    private final Logger log = LoggerFactory.getLogger(PdfTextExtractorServiceImpl.class);

    private final StoredFileRepository storedFileRepository;
    private final StoredFileSearchRepository storedFileSearchRepository;
    
    public PdfTextExtractorServiceImpl(StoredFileRepository storedFileRepository, StoredFileSearchRepository storedFileSearchRepository) {
        this.storedFileRepository = storedFileRepository;
        this.storedFileSearchRepository = storedFileSearchRepository;
    }
     
    @Async
    public void asyncTextExtraction(String fileId, byte[] data) {
    	try {
    		
			PdfReader reader = new PdfReader(data);

			StringBuilder b = new StringBuilder();
			for (int i = 1; i <= reader.getNumberOfPages(); i++) {
				b.append(PdfTextExtractor.getTextFromPage(reader, i));
			}

			StoredFile storedFile = storedFileRepository.findOne(fileId);
			
			storedFile.setContent(b.toString());
			
			storedFileRepository.save(storedFile);
			storedFileSearchRepository.save(storedFile);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
        
}
