package br.gov.pa.prodepa.ps3.service.service;

import org.springframework.beans.factory.annotation.Autowired;

import br.gov.pa.prodepa.ps3.service.domain.AbstractS3Entity;
import br.gov.pa.prodepa.ps3.service.domain.User;
import br.gov.pa.prodepa.ps3.service.repository.UserRepository;
import br.gov.pa.prodepa.ps3.service.security.SecurityUtils;
import br.gov.pa.prodepa.ps3.service.web.rest.errors.InternalServerErrorException;

public abstract class AbstractS3Service {

	@Autowired
	private UserRepository userRepository;

	public void prepareInsert(AbstractS3Entity<?> entity) {
		
		//User Operation Auditing
		final String userLogin = SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new InternalServerErrorException("Current user login not found"));
        User user = userRepository.findOneByLogin(userLogin).orElseThrow(() -> new InternalServerErrorException("Current user login not found"));
        entity.setCreatedBy(user.getId());
        entity.setLastModifiedBy(user.getId());
		
	}
	
}
