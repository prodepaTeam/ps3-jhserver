package br.gov.pa.prodepa.ps3.service.service.dto;


import java.time.Instant;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the StoredFile entity.
 */
public class StoredFileCreateDTO extends AbstractS3DTO {

	private static final long serialVersionUID = 8120167820902805873L;

	private String ownerApplication;

    private String ownerBucket;


    public String getOwnerApplication() {
        return ownerApplication;
    }

    public void setOwnerApplication(String ownerApplication) {
        this.ownerApplication = ownerApplication;
    }

    public String getOwnerBucket() {
        return ownerBucket;
    }

    public void setOwnerBucket(String ownerBucket) {
        this.ownerBucket = ownerBucket;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        StoredFileCreateDTO storedFileDTO = (StoredFileCreateDTO) o;
        if(storedFileDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), storedFileDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "StoredFileDTO{" +
            "id=" + getId() +
            ", ownerApplication=" + getOwnerApplication() +
            ", ownerBucket=" + getOwnerBucket() +
            ", createdAt='" + getCreatedAt() + "'" +
            ", lastModified='" + getLastModified() + "'" +
            ", createdBy=" + getCreatedBy() +
            ", lastModifiedBy=" + getLastModifiedBy() +
            "}";
    }
}
