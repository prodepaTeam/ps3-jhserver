package br.gov.pa.prodepa.ps3.service.security.jwt;

import io.github.jhipster.config.JHipsterProperties;

import java.util.*;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import br.gov.pa.prodepa.ps3.service.security.SecurityUtils;
import io.jsonwebtoken.*;

@Component
public class TokenProvider {

    private final Logger log = LoggerFactory.getLogger(TokenProvider.class);

    private static final String AUTHORITIES_KEY = "auth";
    
    public static final String APPLICATION_ID_KEY = "applicationId";
    public static final String TOKEN_TYPE_KEY = "tokenType";
    public static final String TOKEN_TYPE_SESSION = "tokenTypeSession";
    public static final String TOKEN_TYPE_APPLICATION = "tokenTypeApplication";
    public static final String ENV_ID_KEY = "environmentId";
    public static final String ENV_ID_DESENVOLVER = "DESENVOLVER";
    public static final String ENV_ID_HOMOLOGAR = "HOMOLOGAR";
    public static final String ENV_ID_PRODUCAO = "PRODUCAO";

    private String secretKey;

    private long tokenValidityInMilliseconds;

    private long tokenValidityInMillisecondsForRememberMe;

    private final JHipsterProperties jHipsterProperties;

    public TokenProvider(JHipsterProperties jHipsterProperties) {
        this.jHipsterProperties = jHipsterProperties;
    }

    @PostConstruct
    public void init() {
        this.secretKey =
            jHipsterProperties.getSecurity().getAuthentication().getJwt().getSecret();

        this.tokenValidityInMilliseconds =
            1000 * jHipsterProperties.getSecurity().getAuthentication().getJwt().getTokenValidityInSeconds();
        this.tokenValidityInMillisecondsForRememberMe =
            1000 * jHipsterProperties.getSecurity().getAuthentication().getJwt().getTokenValidityInSecondsForRememberMe();
    }

    public String createToken(Authentication authentication, boolean rememberMe) {
    	
        String authorities = authentication.getAuthorities().stream()
            .map(GrantedAuthority::getAuthority)
            .collect(Collectors.joining(","));

        long now = (new Date()).getTime();
        Date validity;
        
        if (rememberMe) {
            validity = new Date(now + this.tokenValidityInMillisecondsForRememberMe);
        } else {
            validity = new Date(now + this.tokenValidityInMilliseconds);
        }

        return Jwts.builder()
            .setSubject(authentication.getName())
            .claim(AUTHORITIES_KEY, authorities)
            .claim(TOKEN_TYPE_KEY, TOKEN_TYPE_SESSION)
            .claim(ENV_ID_KEY, ENV_ID_DESENVOLVER) //TODO Obter do ambiente
            .signWith(SignatureAlgorithm.HS512, secretKey)
            .setExpiration(validity)
            .compact();
    }
    
    public String createTokenForAppClient(Authentication authentication, String applicationId) {
    	
        String authorities = authentication.getAuthorities().stream()
            .map(GrantedAuthority::getAuthority)
            .collect(Collectors.joining(","));

        String token = 
        Jwts.builder()
            .setSubject(authentication.getName())
            .claim(AUTHORITIES_KEY, authorities)
            .claim(TOKEN_TYPE_KEY, TOKEN_TYPE_APPLICATION)
            .claim(APPLICATION_ID_KEY, applicationId)
            .claim(ENV_ID_KEY, ENV_ID_DESENVOLVER) //TODO Obter do ambiente
            .signWith(SignatureAlgorithm.HS512, secretKey)
            .compact();
        
        //TODO Armazenar token na base :: user.tokens
        
        return token;
    }

    public Authentication getAuthentication(String token) {
    	
        Claims claims = Jwts.parser()
            .setSigningKey(secretKey)
            .parseClaimsJws(token)
            .getBody();

        Collection<? extends GrantedAuthority> authorities =
            Arrays.stream(claims.get(AUTHORITIES_KEY).toString().split(","))
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());

        User principal = new User(claims.getSubject(), "", authorities);

        return new UsernamePasswordAuthenticationToken(principal, token, authorities);
    }

    public boolean validateToken(String authToken) {
    	
        try {
            Jws<Claims> claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(authToken);
            
            String type = (String) claims.getBody().get(TOKEN_TYPE_KEY);
            
            if(type != null && type.equals(TOKEN_TYPE_APPLICATION)) {
            	//TODO Verificar se o token está registrado na base de dados  :: user.tokens
            }
            
            return true;
        } catch (SignatureException e) {
            log.info("Invalid JWT signature.");
            log.trace("Invalid JWT signature trace: {}", e);
        } catch (MalformedJwtException e) {
            log.info("Invalid JWT token.");
            log.trace("Invalid JWT token trace: {}", e);
        } catch (ExpiredJwtException e) {
            log.info("Expired JWT token.");
            log.trace("Expired JWT token trace: {}", e);
        } catch (UnsupportedJwtException e) {
            log.info("Unsupported JWT token.");
            log.trace("Unsupported JWT token trace: {}", e);
        } catch (IllegalArgumentException e) {
            log.info("JWT token compact of handler are invalid.");
            log.trace("JWT token compact of handler are invalid trace: {}", e);
        }
        return false;
    }
    
    public String getClaims(String claimKey) {
    	
    	Optional<String> token = SecurityUtils.getCurrentUserJWT();
    	
        Claims claims = Jwts.parser()
            .setSigningKey(secretKey)
            .parseClaimsJws(token.get())
            .getBody();
        
        if (claims.containsKey(claimKey)) {
        	return claims.get(claimKey, String.class);
        } else {
        	return null;
        }
    }
    
    public Boolean checkClaim(String claimKey, String expectedValue) {
    	
    	String value = getClaims(claimKey);
    	
    	return value != null && value.equals(expectedValue);
    }
}
