package br.gov.pa.prodepa.ps3.service.service.dto;


import java.time.Instant;
import java.io.Serializable;
import java.util.Objects;

/**
 * Abstract DTO for the entities.
 */
public abstract class AbstractS3DTO implements Serializable {

	private static final long serialVersionUID = 7326083758897702133L;

	private String id;

    private Instant createdAt;

    private Instant lastModified;

    private String createdBy;

    private String lastModifiedBy;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Instant getLastModified() {
        return lastModified;
    }

    public void setLastModified(Instant lastModified) {
        this.lastModified = lastModified;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AbstractS3DTO storedFileDTO = (AbstractS3DTO) o;
        if(storedFileDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), storedFileDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "StoredFileDTO{" +
            "id=" + getId() +
            ", createdAt='" + getCreatedAt() + "'" +
            ", lastModified='" + getLastModified() + "'" +
            ", createdBy=" + getCreatedBy() +
            ", lastModifiedBy=" + getLastModifiedBy() +
            "}";
    }
}
