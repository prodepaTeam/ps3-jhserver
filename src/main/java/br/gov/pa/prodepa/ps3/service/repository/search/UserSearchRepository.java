package br.gov.pa.prodepa.ps3.service.repository.search;

import br.gov.pa.prodepa.ps3.service.domain.User;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the User entity.
 */
public interface UserSearchRepository extends ElasticsearchRepository<User, String> {
}
