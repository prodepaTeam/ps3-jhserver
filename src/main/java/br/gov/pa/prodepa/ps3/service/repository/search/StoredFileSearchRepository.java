package br.gov.pa.prodepa.ps3.service.repository.search;

import br.gov.pa.prodepa.ps3.service.domain.StoredFile;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the StoredFile entity.
 */
public interface StoredFileSearchRepository extends ElasticsearchRepository<StoredFile, String> {
}
