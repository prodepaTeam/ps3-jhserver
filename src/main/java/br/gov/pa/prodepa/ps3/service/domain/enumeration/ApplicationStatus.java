package br.gov.pa.prodepa.ps3.service.domain.enumeration;

/**
 * The ApplicationStatus enumeration.
 */
public enum ApplicationStatus {
    ACTIVE, INACTIVE
}
