package br.gov.pa.prodepa.ps3.service.repository;

import br.gov.pa.prodepa.ps3.service.domain.StoredFile;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the StoredFile entity.
 */
@SuppressWarnings("unused")
@Repository
public interface StoredFileRepository extends MongoRepository<StoredFile, String> {

}
