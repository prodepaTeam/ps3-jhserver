package br.gov.pa.prodepa.ps3.service.service.dto;


import java.util.Objects;

/**
 * A DTO for the StoredFile entity.
 */
public class StoredFileDTO extends AbstractS3DTO {

	private static final long serialVersionUID = 8901172999411905992L;

    private String fileId;
    
    private String fileName;
    
    //Return the size of the file in KiloBytes.
    private Long fileLength; 

    private String contentType;

    private String ownerApplicationId;
    private String ownerApplication;

    private String ownerBucketId;
    private String ownerBucket;

    
    public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getOwnerApplication() {
        return ownerApplication;
    }

    public void setOwnerApplication(String ownerApplication) {
        this.ownerApplication = ownerApplication;
    }

    public String getOwnerBucket() {
        return ownerBucket;
    }

    public void setOwnerBucket(String ownerBucket) {
        this.ownerBucket = ownerBucket;
    }
    
    public String getOwnerApplicationId() {
		return ownerApplicationId;
	}

	public void setOwnerApplicationId(String ownerApplicationId) {
		this.ownerApplicationId = ownerApplicationId;
	}
	
	public String getOwnerBucketId() {
		return ownerBucketId;
	}

	public void setOwnerBucketId(String ownerBucketId) {
		this.ownerBucketId = ownerBucketId;
	}

	public Long getFileLength() {
		return fileLength;
	}

	public void setFileLength(Long fileLength) {
		this.fileLength = fileLength;
	}
	
	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        StoredFileDTO storedFileDTO = (StoredFileDTO) o;
        if(storedFileDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), storedFileDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "StoredFileDTO{" +
            "id=" + getId() +
            ", fileName='" + getFileName() + "'" +
            ", mimeType='" + getContentType() + "'" +
            ", ownerApplication=" + getOwnerApplication() +
            ", ownerBucket=" + getOwnerBucket() +
            ", createdAt='" + getCreatedAt() + "'" +
            ", lastModified='" + getLastModified() + "'" +
            ", createdBy=" + getCreatedBy() +
            ", lastModifiedBy=" + getLastModifiedBy() +
            "}";
    }
}
