package br.gov.pa.prodepa.ps3.service.repository;

import br.gov.pa.prodepa.ps3.service.domain.Application;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the Application entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ApplicationRepository extends MongoRepository<Application, String> {

	public Application findOneByName(String name);
}
