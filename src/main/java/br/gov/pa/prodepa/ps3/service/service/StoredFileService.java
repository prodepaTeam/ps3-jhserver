package br.gov.pa.prodepa.ps3.service.service;

import java.io.InputStream;

import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import br.gov.pa.prodepa.ps3.service.service.dto.StoredFileDTO;

/**
 * Service Interface for managing StoredFile.
 */
public interface StoredFileService {

    /**
     * Save a storedFile.
     *
     * @param storedFileDTO the entity to save
     * @param bs 
     * @return the persisted entity
     */
	StoredFileDTO save(StoredFileDTO storedFileDTO, MultipartFile inputStream);

    /**
     * Get all the storedFiles.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<StoredFileDTO> findAll(Pageable pageable);

    /**
     * Get the "id" storedFile.
     *
     * @param id the id of the entity
     * @return the entity
     */
    StoredFileDTO findOne(String id);
    
    
    /**
     * Get the "id" storedFile.
     *
     * @param id the id of the entity
     * @param attachmentOption 
     * @return the entity
     */
    ResponseEntity<InputStreamResource> findOneData(String id, String attachmentOption);

    /**
     * Delete the "id" storedFile.
     *
     * @param id the id of the entity
     */
    void delete(String id);

    /**
     * Search for the storedFile corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<StoredFileDTO> search(String query, Pageable pageable);
}
