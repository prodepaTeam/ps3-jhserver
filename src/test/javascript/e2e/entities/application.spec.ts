import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Application e2e test', () => {

    let navBarPage: NavBarPage;
    let applicationDialogPage: ApplicationDialogPage;
    let applicationComponentsPage: ApplicationComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Applications', () => {
        navBarPage.goToEntity('application');
        applicationComponentsPage = new ApplicationComponentsPage();
        expect(applicationComponentsPage.getTitle())
            .toMatch(/ps3App.application.home.title/);

    });

    it('should load create Application dialog', () => {
        applicationComponentsPage.clickOnCreateButton();
        applicationDialogPage = new ApplicationDialogPage();
        expect(applicationDialogPage.getModalTitle())
            .toMatch(/ps3App.application.home.createOrEditLabel/);
        applicationDialogPage.close();
    });

    it('should create and save Applications', () => {
        applicationComponentsPage.clickOnCreateButton();
        applicationDialogPage.setNameInput('name');
        expect(applicationDialogPage.getNameInput()).toMatch('name');
        applicationDialogPage.statusSelectLastOption();
        applicationDialogPage.setCreatedAtInput(12310020012301);
        expect(applicationDialogPage.getCreatedAtInput()).toMatch('2001-12-31T02:30');
        applicationDialogPage.setLastModifiedInput(12310020012301);
        expect(applicationDialogPage.getLastModifiedInput()).toMatch('2001-12-31T02:30');
        applicationDialogPage.setCreatedByInput('5');
        expect(applicationDialogPage.getCreatedByInput()).toMatch('5');
        applicationDialogPage.setLastModifiedByInput('5');
        expect(applicationDialogPage.getLastModifiedByInput()).toMatch('5');
        applicationDialogPage.save();
        expect(applicationDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class ApplicationComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-application div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ApplicationDialogPage {
    modalTitle = element(by.css('h4#myApplicationLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));
    statusSelect = element(by.css('select#field_status'));
    createdAtInput = element(by.css('input#field_createdAt'));
    lastModifiedInput = element(by.css('input#field_lastModified'));
    createdByInput = element(by.css('input#field_createdBy'));
    lastModifiedByInput = element(by.css('input#field_lastModifiedBy'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameInput = function(name) {
        this.nameInput.sendKeys(name);
    };

    getNameInput = function() {
        return this.nameInput.getAttribute('value');
    };

    setStatusSelect = function(status) {
        this.statusSelect.sendKeys(status);
    };

    getStatusSelect = function() {
        return this.statusSelect.element(by.css('option:checked')).getText();
    };

    statusSelectLastOption = function() {
        this.statusSelect.all(by.tagName('option')).last().click();
    };
    setCreatedAtInput = function(createdAt) {
        this.createdAtInput.sendKeys(createdAt);
    };

    getCreatedAtInput = function() {
        return this.createdAtInput.getAttribute('value');
    };

    setLastModifiedInput = function(lastModified) {
        this.lastModifiedInput.sendKeys(lastModified);
    };

    getLastModifiedInput = function() {
        return this.lastModifiedInput.getAttribute('value');
    };

    setCreatedByInput = function(createdBy) {
        this.createdByInput.sendKeys(createdBy);
    };

    getCreatedByInput = function() {
        return this.createdByInput.getAttribute('value');
    };

    setLastModifiedByInput = function(lastModifiedBy) {
        this.lastModifiedByInput.sendKeys(lastModifiedBy);
    };

    getLastModifiedByInput = function() {
        return this.lastModifiedByInput.getAttribute('value');
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
