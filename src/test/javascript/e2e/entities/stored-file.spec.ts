import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('StoredFile e2e test', () => {

    let navBarPage: NavBarPage;
    let storedFileDialogPage: StoredFileDialogPage;
    let storedFileComponentsPage: StoredFileComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load StoredFiles', () => {
        navBarPage.goToEntity('stored-file');
        storedFileComponentsPage = new StoredFileComponentsPage();
        expect(storedFileComponentsPage.getTitle())
            .toMatch(/ps3App.storedFile.home.title/);

    });

    it('should load create StoredFile dialog', () => {
        storedFileComponentsPage.clickOnCreateButton();
        storedFileDialogPage = new StoredFileDialogPage();
        expect(storedFileDialogPage.getModalTitle())
            .toMatch(/ps3App.storedFile.home.createOrEditLabel/);
        storedFileDialogPage.close();
    });

    it('should create and save StoredFiles', () => {
        storedFileComponentsPage.clickOnCreateButton();
        storedFileDialogPage.setFileNameInput('fileName');
        expect(storedFileDialogPage.getFileNameInput()).toMatch('fileName');
        storedFileDialogPage.setMimeTypeInput('mimeType');
        expect(storedFileDialogPage.getMimeTypeInput()).toMatch('mimeType');
        storedFileDialogPage.setOwnerApplicationInput('5');
        expect(storedFileDialogPage.getOwnerApplicationInput()).toMatch('5');
        storedFileDialogPage.setOwnerBucketInput('5');
        expect(storedFileDialogPage.getOwnerBucketInput()).toMatch('5');
        storedFileDialogPage.setCreatedAtInput(12310020012301);
        expect(storedFileDialogPage.getCreatedAtInput()).toMatch('2001-12-31T02:30');
        storedFileDialogPage.setLastModifiedInput(12310020012301);
        expect(storedFileDialogPage.getLastModifiedInput()).toMatch('2001-12-31T02:30');
        storedFileDialogPage.setCreatedByInput('5');
        expect(storedFileDialogPage.getCreatedByInput()).toMatch('5');
        storedFileDialogPage.setLastModifiedByInput('5');
        expect(storedFileDialogPage.getLastModifiedByInput()).toMatch('5');
        storedFileDialogPage.save();
        expect(storedFileDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class StoredFileComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-stored-file div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class StoredFileDialogPage {
    modalTitle = element(by.css('h4#myStoredFileLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    fileNameInput = element(by.css('input#field_fileName'));
    mimeTypeInput = element(by.css('input#field_mimeType'));
    ownerApplicationInput = element(by.css('input#field_ownerApplication'));
    ownerBucketInput = element(by.css('input#field_ownerBucket'));
    createdAtInput = element(by.css('input#field_createdAt'));
    lastModifiedInput = element(by.css('input#field_lastModified'));
    createdByInput = element(by.css('input#field_createdBy'));
    lastModifiedByInput = element(by.css('input#field_lastModifiedBy'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setFileNameInput = function(fileName) {
        this.fileNameInput.sendKeys(fileName);
    };

    getFileNameInput = function() {
        return this.fileNameInput.getAttribute('value');
    };

    setMimeTypeInput = function(mimeType) {
        this.mimeTypeInput.sendKeys(mimeType);
    };

    getMimeTypeInput = function() {
        return this.mimeTypeInput.getAttribute('value');
    };

    setOwnerApplicationInput = function(ownerApplication) {
        this.ownerApplicationInput.sendKeys(ownerApplication);
    };

    getOwnerApplicationInput = function() {
        return this.ownerApplicationInput.getAttribute('value');
    };

    setOwnerBucketInput = function(ownerBucket) {
        this.ownerBucketInput.sendKeys(ownerBucket);
    };

    getOwnerBucketInput = function() {
        return this.ownerBucketInput.getAttribute('value');
    };

    setCreatedAtInput = function(createdAt) {
        this.createdAtInput.sendKeys(createdAt);
    };

    getCreatedAtInput = function() {
        return this.createdAtInput.getAttribute('value');
    };

    setLastModifiedInput = function(lastModified) {
        this.lastModifiedInput.sendKeys(lastModified);
    };

    getLastModifiedInput = function() {
        return this.lastModifiedInput.getAttribute('value');
    };

    setCreatedByInput = function(createdBy) {
        this.createdByInput.sendKeys(createdBy);
    };

    getCreatedByInput = function() {
        return this.createdByInput.getAttribute('value');
    };

    setLastModifiedByInput = function(lastModifiedBy) {
        this.lastModifiedByInput.sendKeys(lastModifiedBy);
    };

    getLastModifiedByInput = function() {
        return this.lastModifiedByInput.getAttribute('value');
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
