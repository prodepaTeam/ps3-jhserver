import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Bucket e2e test', () => {

    let navBarPage: NavBarPage;
    let bucketDialogPage: BucketDialogPage;
    let bucketComponentsPage: BucketComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Buckets', () => {
        navBarPage.goToEntity('bucket');
        bucketComponentsPage = new BucketComponentsPage();
        expect(bucketComponentsPage.getTitle())
            .toMatch(/ps3App.bucket.home.title/);

    });

    it('should load create Bucket dialog', () => {
        bucketComponentsPage.clickOnCreateButton();
        bucketDialogPage = new BucketDialogPage();
        expect(bucketDialogPage.getModalTitle())
            .toMatch(/ps3App.bucket.home.createOrEditLabel/);
        bucketDialogPage.close();
    });

    it('should create and save Buckets', () => {
        bucketComponentsPage.clickOnCreateButton();
        bucketDialogPage.setNameInput('name');
        expect(bucketDialogPage.getNameInput()).toMatch('name');
        bucketDialogPage.setOwnerApplicationInput('5');
        expect(bucketDialogPage.getOwnerApplicationInput()).toMatch('5');
        bucketDialogPage.accessControlSelectLastOption();
        bucketDialogPage.setCreatedAtInput(12310020012301);
        expect(bucketDialogPage.getCreatedAtInput()).toMatch('2001-12-31T02:30');
        bucketDialogPage.setLastModifiedInput(12310020012301);
        expect(bucketDialogPage.getLastModifiedInput()).toMatch('2001-12-31T02:30');
        bucketDialogPage.setCreatedByInput('5');
        expect(bucketDialogPage.getCreatedByInput()).toMatch('5');
        bucketDialogPage.setLastModifiedByInput('5');
        expect(bucketDialogPage.getLastModifiedByInput()).toMatch('5');
        bucketDialogPage.save();
        expect(bucketDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class BucketComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-bucket div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class BucketDialogPage {
    modalTitle = element(by.css('h4#myBucketLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));
    ownerApplicationInput = element(by.css('input#field_ownerApplication'));
    accessControlSelect = element(by.css('select#field_accessControl'));
    createdAtInput = element(by.css('input#field_createdAt'));
    lastModifiedInput = element(by.css('input#field_lastModified'));
    createdByInput = element(by.css('input#field_createdBy'));
    lastModifiedByInput = element(by.css('input#field_lastModifiedBy'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameInput = function(name) {
        this.nameInput.sendKeys(name);
    };

    getNameInput = function() {
        return this.nameInput.getAttribute('value');
    };

    setOwnerApplicationInput = function(ownerApplication) {
        this.ownerApplicationInput.sendKeys(ownerApplication);
    };

    getOwnerApplicationInput = function() {
        return this.ownerApplicationInput.getAttribute('value');
    };

    setAccessControlSelect = function(accessControl) {
        this.accessControlSelect.sendKeys(accessControl);
    };

    getAccessControlSelect = function() {
        return this.accessControlSelect.element(by.css('option:checked')).getText();
    };

    accessControlSelectLastOption = function() {
        this.accessControlSelect.all(by.tagName('option')).last().click();
    };
    setCreatedAtInput = function(createdAt) {
        this.createdAtInput.sendKeys(createdAt);
    };

    getCreatedAtInput = function() {
        return this.createdAtInput.getAttribute('value');
    };

    setLastModifiedInput = function(lastModified) {
        this.lastModifiedInput.sendKeys(lastModified);
    };

    getLastModifiedInput = function() {
        return this.lastModifiedInput.getAttribute('value');
    };

    setCreatedByInput = function(createdBy) {
        this.createdByInput.sendKeys(createdBy);
    };

    getCreatedByInput = function() {
        return this.createdByInput.getAttribute('value');
    };

    setLastModifiedByInput = function(lastModifiedBy) {
        this.lastModifiedByInput.sendKeys(lastModifiedBy);
    };

    getLastModifiedByInput = function() {
        return this.lastModifiedByInput.getAttribute('value');
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
