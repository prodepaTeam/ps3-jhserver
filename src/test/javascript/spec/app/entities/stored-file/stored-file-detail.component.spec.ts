/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { Ps3TestModule } from '../../../test.module';
import { StoredFileDetailComponent } from '../../../../../../main/webapp/app/entities/stored-file/stored-file-detail.component';
import { StoredFileService } from '../../../../../../main/webapp/app/entities/stored-file/stored-file.service';
import { StoredFile } from '../../../../../../main/webapp/app/entities/stored-file/stored-file.model';

describe('Component Tests', () => {

    describe('StoredFile Management Detail Component', () => {
        let comp: StoredFileDetailComponent;
        let fixture: ComponentFixture<StoredFileDetailComponent>;
        let service: StoredFileService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [Ps3TestModule],
                declarations: [StoredFileDetailComponent],
                providers: [
                    StoredFileService
                ]
            })
            .overrideTemplate(StoredFileDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(StoredFileDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(StoredFileService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new StoredFile('123')
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith('123');
                expect(comp.storedFile).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
