/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { Ps3TestModule } from '../../../test.module';
import { StoredFileComponent } from '../../../../../../main/webapp/app/entities/stored-file/stored-file.component';
import { StoredFileService } from '../../../../../../main/webapp/app/entities/stored-file/stored-file.service';
import { StoredFile } from '../../../../../../main/webapp/app/entities/stored-file/stored-file.model';

describe('Component Tests', () => {

    describe('StoredFile Management Component', () => {
        let comp: StoredFileComponent;
        let fixture: ComponentFixture<StoredFileComponent>;
        let service: StoredFileService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [Ps3TestModule],
                declarations: [StoredFileComponent],
                providers: [
                    StoredFileService
                ]
            })
            .overrideTemplate(StoredFileComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(StoredFileComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(StoredFileService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new StoredFile('123')],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.storedFiles[0]).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
