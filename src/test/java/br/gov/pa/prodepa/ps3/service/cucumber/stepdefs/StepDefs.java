package br.gov.pa.prodepa.ps3.service.cucumber.stepdefs;

import br.gov.pa.prodepa.ps3.service.Ps3App;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import org.springframework.boot.test.context.SpringBootTest;

@WebAppConfiguration
@SpringBootTest
@ContextConfiguration(classes = Ps3App.class)
public abstract class StepDefs {

    protected ResultActions actions;

}
