package br.gov.pa.prodepa.ps3.service.web.rest;

import br.gov.pa.prodepa.ps3.service.Ps3App;
import br.gov.pa.prodepa.ps3.service.domain.Application;
import br.gov.pa.prodepa.ps3.service.domain.Bucket;
import br.gov.pa.prodepa.ps3.service.repository.BucketRepository;
import br.gov.pa.prodepa.ps3.service.service.BucketService;
import br.gov.pa.prodepa.ps3.service.repository.search.BucketSearchRepository;
import br.gov.pa.prodepa.ps3.service.service.dto.BucketDTO;
import br.gov.pa.prodepa.ps3.service.service.mapper.BucketMapper;
import br.gov.pa.prodepa.ps3.service.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static br.gov.pa.prodepa.ps3.service.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import br.gov.pa.prodepa.ps3.service.domain.enumeration.AccessControl;
import br.gov.pa.prodepa.ps3.service.domain.refs.ApplicationRef;
/**
 * Test class for the BucketResource REST controller.
 *
 * @see BucketResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Ps3App.class)
public class BucketResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_OWNER_APPLICATION = "1";
    private static final String UPDATED_OWNER_APPLICATION = "2";

    private static final AccessControl DEFAULT_ACCESS_CONTROL = AccessControl.PUBLIC;
    private static final AccessControl UPDATED_ACCESS_CONTROL = AccessControl.USER;

    private static final Instant DEFAULT_CREATED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_LAST_MODIFIED = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "1";
    private static final String UPDATED_CREATED_BY = "2";

    private static final String DEFAULT_LAST_MODIFIED_BY = "1";
    private static final String UPDATED_LAST_MODIFIED_BY = "2";

    @Autowired
    private BucketRepository bucketRepository;

    @Autowired
    private BucketMapper bucketMapper;

    @Autowired
    private BucketService bucketService;

    @Autowired
    private BucketSearchRepository bucketSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restBucketMockMvc;

    private Bucket bucket;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BucketResource bucketResource = new BucketResource(bucketService);
        this.restBucketMockMvc = MockMvcBuilders.standaloneSetup(bucketResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Bucket createEntity() {
        Bucket bucket = new Bucket()
            .name(DEFAULT_NAME)
            .ownerApplication(new ApplicationRef(DEFAULT_OWNER_APPLICATION))
            .accessControl(DEFAULT_ACCESS_CONTROL)
            .createdAt(DEFAULT_CREATED_AT)
            .lastModified(DEFAULT_LAST_MODIFIED)
            .createdBy(DEFAULT_CREATED_BY)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY);
        return bucket;
    }

    @Before
    public void initTest() {
        bucketRepository.deleteAll();
        bucketSearchRepository.deleteAll();
        bucket = createEntity();
    }

    @Test
    public void createBucket() throws Exception {
        int databaseSizeBeforeCreate = bucketRepository.findAll().size();

        // Create the Bucket
        BucketDTO bucketDTO = bucketMapper.toDto(bucket);
        restBucketMockMvc.perform(post("/api/buckets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bucketDTO)))
            .andExpect(status().isCreated());

        // Validate the Bucket in the database
        List<Bucket> bucketList = bucketRepository.findAll();
        assertThat(bucketList).hasSize(databaseSizeBeforeCreate + 1);
        Bucket testBucket = bucketList.get(bucketList.size() - 1);
        assertThat(testBucket.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testBucket.getOwnerApplication()).isEqualTo(DEFAULT_OWNER_APPLICATION);
        assertThat(testBucket.getAccessControl()).isEqualTo(DEFAULT_ACCESS_CONTROL);
        assertThat(testBucket.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testBucket.getLastModified()).isEqualTo(DEFAULT_LAST_MODIFIED);
        assertThat(testBucket.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testBucket.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);

        // Validate the Bucket in Elasticsearch
        Bucket bucketEs = bucketSearchRepository.findOne(testBucket.getId());
        assertThat(bucketEs).isEqualToIgnoringGivenFields(testBucket);
    }

    @Test
    public void createBucketWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = bucketRepository.findAll().size();

        // Create the Bucket with an existing ID
        bucket.setId("existing_id");
        BucketDTO bucketDTO = bucketMapper.toDto(bucket);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBucketMockMvc.perform(post("/api/buckets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bucketDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Bucket in the database
        List<Bucket> bucketList = bucketRepository.findAll();
        assertThat(bucketList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void getAllBuckets() throws Exception {
        // Initialize the database
        bucketRepository.save(bucket);

        // Get all the bucketList
        restBucketMockMvc.perform(get("/api/buckets?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bucket.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].ownerApplication").value(hasItem(DEFAULT_OWNER_APPLICATION)))
            .andExpect(jsonPath("$.[*].accessControl").value(hasItem(DEFAULT_ACCESS_CONTROL.toString())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].lastModified").value(hasItem(DEFAULT_LAST_MODIFIED.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)));
    }

    @Test
    public void getBucket() throws Exception {
        // Initialize the database
        bucketRepository.save(bucket);

        // Get the bucket
        restBucketMockMvc.perform(get("/api/buckets/{id}", bucket.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(bucket.getId()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.ownerApplication").value(DEFAULT_OWNER_APPLICATION))
            .andExpect(jsonPath("$.accessControl").value(DEFAULT_ACCESS_CONTROL.toString()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT.toString()))
            .andExpect(jsonPath("$.lastModified").value(DEFAULT_LAST_MODIFIED.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY));
    }

    @Test
    public void getNonExistingBucket() throws Exception {
        // Get the bucket
        restBucketMockMvc.perform(get("/api/buckets/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateBucket() throws Exception {
        // Initialize the database
        bucketRepository.save(bucket);
        bucketSearchRepository.save(bucket);
        int databaseSizeBeforeUpdate = bucketRepository.findAll().size();

        // Update the bucket
        Bucket updatedBucket = bucketRepository.findOne(bucket.getId());
        updatedBucket
            .name(UPDATED_NAME)
            .ownerApplication(new ApplicationRef(UPDATED_OWNER_APPLICATION))
            .accessControl(UPDATED_ACCESS_CONTROL)
            .createdAt(UPDATED_CREATED_AT)
            .lastModified(UPDATED_LAST_MODIFIED)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY);
        BucketDTO bucketDTO = bucketMapper.toDto(updatedBucket);

        restBucketMockMvc.perform(put("/api/buckets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bucketDTO)))
            .andExpect(status().isOk());

        // Validate the Bucket in the database
        List<Bucket> bucketList = bucketRepository.findAll();
        assertThat(bucketList).hasSize(databaseSizeBeforeUpdate);
        Bucket testBucket = bucketList.get(bucketList.size() - 1);
        assertThat(testBucket.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testBucket.getOwnerApplication()).isEqualTo(UPDATED_OWNER_APPLICATION);
        assertThat(testBucket.getAccessControl()).isEqualTo(UPDATED_ACCESS_CONTROL);
        assertThat(testBucket.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testBucket.getLastModified()).isEqualTo(UPDATED_LAST_MODIFIED);
        assertThat(testBucket.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testBucket.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);

        // Validate the Bucket in Elasticsearch
        Bucket bucketEs = bucketSearchRepository.findOne(testBucket.getId());
        assertThat(bucketEs).isEqualToIgnoringGivenFields(testBucket);
    }

    @Test
    public void updateNonExistingBucket() throws Exception {
        int databaseSizeBeforeUpdate = bucketRepository.findAll().size();

        // Create the Bucket
        BucketDTO bucketDTO = bucketMapper.toDto(bucket);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restBucketMockMvc.perform(put("/api/buckets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bucketDTO)))
            .andExpect(status().isCreated());

        // Validate the Bucket in the database
        List<Bucket> bucketList = bucketRepository.findAll();
        assertThat(bucketList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteBucket() throws Exception {
        // Initialize the database
        bucketRepository.save(bucket);
        bucketSearchRepository.save(bucket);
        int databaseSizeBeforeDelete = bucketRepository.findAll().size();

        // Get the bucket
        restBucketMockMvc.perform(delete("/api/buckets/{id}", bucket.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean bucketExistsInEs = bucketSearchRepository.exists(bucket.getId());
        assertThat(bucketExistsInEs).isFalse();

        // Validate the database is empty
        List<Bucket> bucketList = bucketRepository.findAll();
        assertThat(bucketList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void searchBucket() throws Exception {
        // Initialize the database
        bucketRepository.save(bucket);
        bucketSearchRepository.save(bucket);

        // Search the bucket
        restBucketMockMvc.perform(get("/api/_search/buckets?query=id:" + bucket.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bucket.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].ownerApplication").value(hasItem(DEFAULT_OWNER_APPLICATION)))
            .andExpect(jsonPath("$.[*].accessControl").value(hasItem(DEFAULT_ACCESS_CONTROL.toString())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].lastModified").value(hasItem(DEFAULT_LAST_MODIFIED.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)));
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Bucket.class);
        Bucket bucket1 = new Bucket();
        bucket1.setId("id1");
        Bucket bucket2 = new Bucket();
        bucket2.setId(bucket1.getId());
        assertThat(bucket1).isEqualTo(bucket2);
        bucket2.setId("id2");
        assertThat(bucket1).isNotEqualTo(bucket2);
        bucket1.setId(null);
        assertThat(bucket1).isNotEqualTo(bucket2);
    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BucketDTO.class);
        BucketDTO bucketDTO1 = new BucketDTO();
        bucketDTO1.setId("id1");
        BucketDTO bucketDTO2 = new BucketDTO();
        assertThat(bucketDTO1).isNotEqualTo(bucketDTO2);
        bucketDTO2.setId(bucketDTO1.getId());
        assertThat(bucketDTO1).isEqualTo(bucketDTO2);
        bucketDTO2.setId("id2");
        assertThat(bucketDTO1).isNotEqualTo(bucketDTO2);
        bucketDTO1.setId(null);
        assertThat(bucketDTO1).isNotEqualTo(bucketDTO2);
    }
}
