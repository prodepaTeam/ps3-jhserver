package br.gov.pa.prodepa.ps3.service.web.rest.client;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

import org.junit.Test;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.annotation.JsonProperty;

import br.gov.pa.prodepa.ps3.service.web.rest.UserJWTController;
import br.gov.pa.prodepa.ps3.service.web.rest.vm.LoginVM;
import gherkin.deps.com.google.gson.JsonObject;

//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = Ps3App.class)
public class ClientStoreResourceSimpleTest {

	
	final String PATH = "http://localhost:8080/client/api/stored-files";
	
	final String appToken = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImFwcGxpY2F0aW9uSWQiOiJFUFJPVE9DT0xPIiwiZW52aXJvbm1lbnRJZCI6IkRFU0VOVk9MVkVSIn0.EdHHz0IvcQq7FLNWWkylf233YqTzyO5Xzj60dhAlY6-sTnxMCfARUnbQA8OfNWOaF_o52mG1kjuX4ZPV4Z3j9A";

	@Test
	public void testClientStoreResource() {
		fail("Not yet implemented");
	}

	@Test
	public void testStoredFile() {
		
		//https://tamasgyorfi.net/2015/03/27/posting-multipart-requests-with-resttemplate/
		//https://gist.github.com/ansidev/5816b8b3108c30b5279f5fec67506798
		
		FileSystemResource value = new FileSystemResource(new File("/home/thiago/Downloads/arquivo01.pdf"));

		LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
		map.add("bucketId", "bk001");
        map.add("file", value);
        
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.set("Authorization", appToken);
        HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity = new HttpEntity<>(map, headers);
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.exchange(PATH + "?appId=eProt", HttpMethod.POST, requestEntity, String.class);
		
	}

	@Test
	public void testGetStoredFile() {
		RestTemplate restTemplate = new RestTemplate();
		String ok = restTemplate.getForObject(PATH + "/5b2167d12103090f6210b9cd", String.class);
		System.out.println(ok);
	}
	
	@Test
	public void testGetStoredFileData() throws IOException {
		RestTemplate restTemplate = new RestTemplate();
		
		HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM));

	    HttpEntity<String> entity = new HttpEntity<String>(headers);

	    ResponseEntity<byte[]> response = restTemplate.exchange(PATH + "/5b227eaf2103094de242f0cf/data", HttpMethod.GET, entity, byte[].class, "1");

	    if (response.getStatusCode() == HttpStatus.OK) {
	        Files.write(Paths.get("/home/thiago/Downloads/TMP/teste.pdf"), response.getBody());
	    }
		
	}

	@Test
	public void testUpdateStoredFile() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetAllStoredFiles() {
		fail("Not yet implemented");
	}


	@Test
	public void testDeleteStoredFile() {
		fail("Not yet implemented");
	}

	@Test
	public void testSearchStoredFiles() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetTokenApp() throws Exception {

		LoginVM loginVM = new LoginVM();
		loginVM.setUsername("admin");
		loginVM.setPassword("admin");
		loginVM.setApplicationId("eProtocolo");
		
		//LOCAL
		//String token = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsInRva2VuVHlwZSI6InRva2VuVHlwZVNlc3Npb24iLCJlbnZpcm9ubWVudElkIjoiREVTRU5WT0xWRVIiLCJleHAiOjE1Mjk0MTU2MDV9.AG9-lFCO6Jhtei0JTIOJY6s-9U0tHw_ClkpObqo4vOtjbiELac6Ario5zGscQoIsv0zWDd7aYJM0xHy0yVsQFw";
		
		//docker REMOTO
		String token = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsInRva2VuVHlwZSI6InRva2VuVHlwZVNlc3Npb24iLCJlbnZpcm9ubWVudElkIjoiREVTRU5WT0xWRVIiLCJleHAiOjE1Mjk0MTYxODZ9.muK9UDOCeLNNSj5wz4wDnismeXeTB516fZXRp3KM1HsVyaK5RqVn40p5R3mAui7gsOmRSoL0Aixa00gMmGU_TQ";
		HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", token);
        
        
        HttpEntity<LoginVM> request = new HttpEntity<LoginVM>(loginVM, headers);
		
        RestTemplate restTemplateAuth = new RestTemplate();
        //JWTToken tk = restTemplateAuth.postForObject("http://localhost:8080/api/authenticate/client", request, JWTToken.class);
        JWTToken tk = restTemplateAuth.postForObject("http://10.1.2.71/api/authenticate/client", request, JWTToken.class);
        
        System.out.println(tk.getIdToken());
	}
	
	
	@Test
	public void testPing() throws Exception {

		/*LoginVM loginVM = new LoginVM();
		loginVM.setUsername("admin");
		loginVM.setPassword("admin");
		
        RestTemplate restTemplateAuth = new RestTemplate();
        JWTToken tk = restTemplateAuth.postForObject("http://localhost:8080/api/authenticate", loginVM, JWTToken.class);
        
        System.out.println(tk.getIdToken());
        
        String token = "Bearer " + tk.getIdToken();
        */
		
		String token = appToken;
        //String token = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTUyOTA2NTAwNH0.PrHlqxFPu643rHEVfbVlGmOiRhMCStfOv92dhhuUc3V-G8dkF_P6zjxnBU6bo_xAWN9YUWZXTXxRJVYImaRznQ";
		
		HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", token);
		
        RestTemplate restTemplate = new RestTemplate();
        
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        ResponseEntity<String> response = restTemplate.exchange("http://localhost:8080/client/api/ping/stored-files", HttpMethod.GET, entity, String.class);
        System.out.println("Result - status ("+ response.getStatusCode() + ") has body: " + response.hasBody());
        
        System.out.println(response);
	}
	
	
	
	 static class JWTToken {

	        private String idToken;

	        
	        public JWTToken() {
	        }
	        
	        public JWTToken(String idToken) {
	            this.idToken = idToken;
	        }

	        @JsonProperty("id_token")
	        String getIdToken() {
	            return idToken;
	        }

	        void setIdToken(String idToken) {
	            this.idToken = idToken;
	        }
	    }

}
