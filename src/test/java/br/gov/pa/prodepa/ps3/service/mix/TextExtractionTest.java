package br.gov.pa.prodepa.ps3.service.mix;


import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.pdf.PDFParser;
import org.apache.tika.sax.BodyContentHandler;
import org.junit.Test;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.options;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;


/**
 * Unit tests for the WebConfigurer class.
 *
 * @see WebConfigurer
 */
public class TextExtractionTest {

	@Test
    public void testIText() {
        System.out.println("ITextText >>>>>>>>>>>>>.");
        
        long start = System.currentTimeMillis();
      
        
        try {

            //PdfReader reader = new PdfReader("/home/thiago/Downloads/dem_remun_pessoal_mar_2018_parte_2.pdf");
            PdfReader reader = new PdfReader("/home/thiago/Downloads/arquivo01.pdf");
            System.out.println("This PDF has "+reader.getNumberOfPages()+" pages.");
            
            System.out.println(System.currentTimeMillis() - start);
            
            StringBuilder b = new StringBuilder();
            for (int i = 1; i <= reader.getNumberOfPages(); i++) {
            	b.append( PdfTextExtractor.getTextFromPage(reader, 1) );
				
			}
            
            //System.out.println("Page Content:\n\n"+b.toString()+"\n\n");
            System.out.println("Is this document tampered: "+reader.isTampered());
            System.out.println("Is this document encrypted: "+reader.isEncrypted());
            
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("\n\n\n\n#################");
        System.out.println(System.currentTimeMillis() - start);
        System.out.println("#################\n\n\n\n");
    }
    
    
    @Test
    public void testPdfBox() throws Exception {
    	System.out.println("PDFBox >>>>>>>>>>>>>.");
    	
    	 long start = System.currentTimeMillis();
    	
    	 //Loading an existing document
        //File file = new File("/home/thiago/Downloads/dem_remun_pessoal_mar_2018_parte_2.pdf");
        File file = new File("/home/thiago/Downloads/arquivo01.pdf");
        PDDocument document = PDDocument.load(file);

        System.out.println(System.currentTimeMillis() - start);
        
        //Instantiate PDFTextStripper class
        PDFTextStripper pdfStripper = new PDFTextStripper();

        //Retrieving text from PDF document
        String text = pdfStripper.getText(document);
        //System.out.println(text);

        //Closing the document
        document.close();
        
        System.out.println("\n\n\n\n#################");
        System.out.println(System.currentTimeMillis() - start);
        System.out.println("#################\n\n\n\n");
        document.close();
    }
    
    
    @Test
    public void testTike() throws Exception {
    	System.out.println("Tika >>>>>>>>>>>>>.");
    	
    	long start = System.currentTimeMillis();
    	
    	
    	/*FileInputStream input = new FileInputStream("/home/thiago/Downloads/dem_remun_pessoal_mar_2018_parte_2.pdf");
		try {
            //input = entity.getContent();
            BodyContentHandler handler = new BodyContentHandler();
            Metadata metadata = new Metadata();
            AutoDetectParser parser = new AutoDetectParser();
            ParseContext parseContext = new ParseContext();
            parser.parse(input , handler, metadata, parseContext);
            
            System.out.println(handler.toString());
            
            //map.put("text", handler.toString().replaceAll("\n|\r|\t", " "));
            //map.put("title", metadata.get(TikaCoreProperties.TITLE));
            //map.put("pageCount", metadata.get("xmpTPg:NPages"));
            //map.put("status_code", response.getStatusLine().getStatusCode() + "");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }*/
    	
    	
    	BodyContentHandler handler = new BodyContentHandler();
        Metadata metadata = new Metadata();
        //FileInputStream inputstream = new FileInputStream(new File("/home/thiago/Downloads/dem_remun_pessoal_mar_2018_parte_2.pdf"));
        FileInputStream inputstream = new FileInputStream(new File("/home/thiago/Downloads/arquivo01.pdf"));
        ParseContext pcontext = new ParseContext();
        
        //parsing the document using PDF parser
        PDFParser pdfparser = new PDFParser();
        pdfparser.parse(inputstream, handler, metadata,pcontext);
        
        //getting the content of the document
        //System.out.println("Contents of the PDF :" + handler.toString());
        
        //getting metadata of the document
        System.out.println("Metadata of the PDF:");
        String[] metadataNames = metadata.names();
        
        /*for(String name : metadataNames) {
           System.out.println(name+ " : " + metadata.get(name));
        }*/
    	
        inputstream.close();
    	
        
        System.out.println("\n\n\n\n#################");
        System.out.println(System.currentTimeMillis() - start);
        System.out.println("#################\n\n\n\n");
    }

}
