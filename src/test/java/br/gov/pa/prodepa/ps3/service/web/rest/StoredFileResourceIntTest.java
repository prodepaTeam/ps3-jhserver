package br.gov.pa.prodepa.ps3.service.web.rest;

import br.gov.pa.prodepa.ps3.service.Ps3App;

import br.gov.pa.prodepa.ps3.service.domain.StoredFile;
import br.gov.pa.prodepa.ps3.service.domain.refs.ApplicationRef;
import br.gov.pa.prodepa.ps3.service.domain.refs.BucketRef;
import br.gov.pa.prodepa.ps3.service.repository.StoredFileRepository;
import br.gov.pa.prodepa.ps3.service.service.StoredFileService;
import br.gov.pa.prodepa.ps3.service.repository.search.StoredFileSearchRepository;
import br.gov.pa.prodepa.ps3.service.service.dto.StoredFileDTO;
import br.gov.pa.prodepa.ps3.service.service.mapper.StoredFileMapper;
import br.gov.pa.prodepa.ps3.service.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static br.gov.pa.prodepa.ps3.service.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the StoredFileResource REST controller.
 *
 * @see StoredFileResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Ps3App.class)
public class StoredFileResourceIntTest {

    private static final String DEFAULT_FILE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FILE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_MIME_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_MIME_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_OWNER_APPLICATION = "1";
    private static final String UPDATED_OWNER_APPLICATION = "2";

    private static final String DEFAULT_OWNER_BUCKET = "1";
    private static final String UPDATED_OWNER_BUCKET = "2";

    private static final Instant DEFAULT_CREATED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_LAST_MODIFIED = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "1";
    private static final String UPDATED_CREATED_BY = "1";

    private static final String DEFAULT_LAST_MODIFIED_BY = "1";
    private static final String UPDATED_LAST_MODIFIED_BY = "2";

    @Autowired
    private StoredFileRepository storedFileRepository;

    @Autowired
    private StoredFileMapper storedFileMapper;

    @Autowired
    private StoredFileService storedFileService;

    @Autowired
    private StoredFileSearchRepository storedFileSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restStoredFileMockMvc;

    private StoredFile storedFile;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final StoredFileResource storedFileResource = new StoredFileResource(storedFileService);
        this.restStoredFileMockMvc = MockMvcBuilders.standaloneSetup(storedFileResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static StoredFile createEntity() {
        StoredFile storedFile = new StoredFile()
            .fileName(DEFAULT_FILE_NAME)
            .contentType(DEFAULT_MIME_TYPE)
            .ownerApplication(new ApplicationRef(DEFAULT_OWNER_APPLICATION))
            .ownerBucket(new BucketRef(DEFAULT_OWNER_BUCKET))
            .createdAt(DEFAULT_CREATED_AT)
            .lastModified(DEFAULT_LAST_MODIFIED)
            .createdBy(DEFAULT_CREATED_BY)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY);
        return storedFile;
    }

    @Before
    public void initTest() {
        storedFileRepository.deleteAll();
        storedFileSearchRepository.deleteAll();
        storedFile = createEntity();
    }

    @Test
    public void createStoredFile() throws Exception {
        int databaseSizeBeforeCreate = storedFileRepository.findAll().size();

        // Create the StoredFile
        StoredFileDTO storedFileDTO = storedFileMapper.toDto(storedFile);
        restStoredFileMockMvc.perform(post("/api/stored-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(storedFileDTO)))
            .andExpect(status().isCreated());

        // Validate the StoredFile in the database
        List<StoredFile> storedFileList = storedFileRepository.findAll();
        assertThat(storedFileList).hasSize(databaseSizeBeforeCreate + 1);
        StoredFile testStoredFile = storedFileList.get(storedFileList.size() - 1);
        assertThat(testStoredFile.getFileName()).isEqualTo(DEFAULT_FILE_NAME);
        assertThat(testStoredFile.getContentType()).isEqualTo(DEFAULT_MIME_TYPE);
        assertThat(testStoredFile.getOwnerApplication()).isEqualTo(DEFAULT_OWNER_APPLICATION);
        assertThat(testStoredFile.getOwnerBucket()).isEqualTo(DEFAULT_OWNER_BUCKET);
        assertThat(testStoredFile.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testStoredFile.getLastModified()).isEqualTo(DEFAULT_LAST_MODIFIED);
        assertThat(testStoredFile.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testStoredFile.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);

        // Validate the StoredFile in Elasticsearch
        StoredFile storedFileEs = storedFileSearchRepository.findOne(testStoredFile.getId());
        assertThat(storedFileEs).isEqualToIgnoringGivenFields(testStoredFile);
    }

    @Test
    public void createStoredFileWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = storedFileRepository.findAll().size();

        // Create the StoredFile with an existing ID
        storedFile.setId("existing_id");
        StoredFileDTO storedFileDTO = storedFileMapper.toDto(storedFile);

        // An entity with an existing ID cannot be created, so this API call must fail
        restStoredFileMockMvc.perform(post("/api/stored-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(storedFileDTO)))
            .andExpect(status().isBadRequest());

        // Validate the StoredFile in the database
        List<StoredFile> storedFileList = storedFileRepository.findAll();
        assertThat(storedFileList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void getAllStoredFiles() throws Exception {
        // Initialize the database
        storedFileRepository.save(storedFile);

        // Get all the storedFileList
        restStoredFileMockMvc.perform(get("/api/stored-files?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(storedFile.getId())))
            .andExpect(jsonPath("$.[*].fileName").value(hasItem(DEFAULT_FILE_NAME.toString())))
            .andExpect(jsonPath("$.[*].mimeType").value(hasItem(DEFAULT_MIME_TYPE.toString())))
            .andExpect(jsonPath("$.[*].ownerApplication").value(hasItem(DEFAULT_OWNER_APPLICATION)))
            .andExpect(jsonPath("$.[*].ownerBucket").value(hasItem(DEFAULT_OWNER_BUCKET)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].lastModified").value(hasItem(DEFAULT_LAST_MODIFIED.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)));
    }

    @Test
    public void getStoredFile() throws Exception {
        // Initialize the database
        storedFileRepository.save(storedFile);

        // Get the storedFile
        restStoredFileMockMvc.perform(get("/api/stored-files/{id}", storedFile.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(storedFile.getId()))
            .andExpect(jsonPath("$.fileName").value(DEFAULT_FILE_NAME.toString()))
            .andExpect(jsonPath("$.mimeType").value(DEFAULT_MIME_TYPE.toString()))
            .andExpect(jsonPath("$.ownerApplication").value(DEFAULT_OWNER_APPLICATION))
            .andExpect(jsonPath("$.ownerBucket").value(DEFAULT_OWNER_BUCKET))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT.toString()))
            .andExpect(jsonPath("$.lastModified").value(DEFAULT_LAST_MODIFIED.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY));
    }

    @Test
    public void getNonExistingStoredFile() throws Exception {
        // Get the storedFile
        restStoredFileMockMvc.perform(get("/api/stored-files/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateStoredFile() throws Exception {
        // Initialize the database
        storedFileRepository.save(storedFile);
        storedFileSearchRepository.save(storedFile);
        int databaseSizeBeforeUpdate = storedFileRepository.findAll().size();

        // Update the storedFile
        StoredFile updatedStoredFile = storedFileRepository.findOne(storedFile.getId());
        updatedStoredFile
            .fileName(UPDATED_FILE_NAME)
            .contentType(UPDATED_MIME_TYPE)
            .ownerApplication(new ApplicationRef(UPDATED_OWNER_APPLICATION))
            .ownerBucket(new BucketRef(UPDATED_OWNER_BUCKET))
            .createdAt(UPDATED_CREATED_AT)
            .lastModified(UPDATED_LAST_MODIFIED)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY);
        StoredFileDTO storedFileDTO = storedFileMapper.toDto(updatedStoredFile);

        restStoredFileMockMvc.perform(put("/api/stored-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(storedFileDTO)))
            .andExpect(status().isOk());

        // Validate the StoredFile in the database
        List<StoredFile> storedFileList = storedFileRepository.findAll();
        assertThat(storedFileList).hasSize(databaseSizeBeforeUpdate);
        StoredFile testStoredFile = storedFileList.get(storedFileList.size() - 1);
        assertThat(testStoredFile.getFileName()).isEqualTo(UPDATED_FILE_NAME);
        assertThat(testStoredFile.getContentType()).isEqualTo(UPDATED_MIME_TYPE);
        assertThat(testStoredFile.getOwnerApplication()).isEqualTo(UPDATED_OWNER_APPLICATION);
        assertThat(testStoredFile.getOwnerBucket()).isEqualTo(UPDATED_OWNER_BUCKET);
        assertThat(testStoredFile.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testStoredFile.getLastModified()).isEqualTo(UPDATED_LAST_MODIFIED);
        assertThat(testStoredFile.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testStoredFile.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);

        // Validate the StoredFile in Elasticsearch
        StoredFile storedFileEs = storedFileSearchRepository.findOne(testStoredFile.getId());
        assertThat(storedFileEs).isEqualToIgnoringGivenFields(testStoredFile);
    }

    @Test
    public void updateNonExistingStoredFile() throws Exception {
        int databaseSizeBeforeUpdate = storedFileRepository.findAll().size();

        // Create the StoredFile
        StoredFileDTO storedFileDTO = storedFileMapper.toDto(storedFile);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restStoredFileMockMvc.perform(put("/api/stored-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(storedFileDTO)))
            .andExpect(status().isCreated());

        // Validate the StoredFile in the database
        List<StoredFile> storedFileList = storedFileRepository.findAll();
        assertThat(storedFileList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteStoredFile() throws Exception {
        // Initialize the database
        storedFileRepository.save(storedFile);
        storedFileSearchRepository.save(storedFile);
        int databaseSizeBeforeDelete = storedFileRepository.findAll().size();

        // Get the storedFile
        restStoredFileMockMvc.perform(delete("/api/stored-files/{id}", storedFile.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean storedFileExistsInEs = storedFileSearchRepository.exists(storedFile.getId());
        assertThat(storedFileExistsInEs).isFalse();

        // Validate the database is empty
        List<StoredFile> storedFileList = storedFileRepository.findAll();
        assertThat(storedFileList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void searchStoredFile() throws Exception {
        // Initialize the database
        storedFileRepository.save(storedFile);
        storedFileSearchRepository.save(storedFile);

        // Search the storedFile
        restStoredFileMockMvc.perform(get("/api/_search/stored-files?query=id:" + storedFile.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(storedFile.getId())))
            .andExpect(jsonPath("$.[*].fileName").value(hasItem(DEFAULT_FILE_NAME.toString())))
            .andExpect(jsonPath("$.[*].mimeType").value(hasItem(DEFAULT_MIME_TYPE.toString())))
            .andExpect(jsonPath("$.[*].ownerApplication").value(hasItem(DEFAULT_OWNER_APPLICATION)))
            .andExpect(jsonPath("$.[*].ownerBucket").value(hasItem(DEFAULT_OWNER_BUCKET)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].lastModified").value(hasItem(DEFAULT_LAST_MODIFIED.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)));
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(StoredFile.class);
        StoredFile storedFile1 = new StoredFile();
        storedFile1.setId("id1");
        StoredFile storedFile2 = new StoredFile();
        storedFile2.setId(storedFile1.getId());
        assertThat(storedFile1).isEqualTo(storedFile2);
        storedFile2.setId("id2");
        assertThat(storedFile1).isNotEqualTo(storedFile2);
        storedFile1.setId(null);
        assertThat(storedFile1).isNotEqualTo(storedFile2);
    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(StoredFileDTO.class);
        StoredFileDTO storedFileDTO1 = new StoredFileDTO();
        storedFileDTO1.setId("id1");
        StoredFileDTO storedFileDTO2 = new StoredFileDTO();
        assertThat(storedFileDTO1).isNotEqualTo(storedFileDTO2);
        storedFileDTO2.setId(storedFileDTO1.getId());
        assertThat(storedFileDTO1).isEqualTo(storedFileDTO2);
        storedFileDTO2.setId("id2");
        assertThat(storedFileDTO1).isNotEqualTo(storedFileDTO2);
        storedFileDTO1.setId(null);
        assertThat(storedFileDTO1).isNotEqualTo(storedFileDTO2);
    }
}
