package br.gov.pa.prodepa.ps3.service.web.rest.client;

import static br.gov.pa.prodepa.ps3.service.web.rest.TestUtil.createFormattingConversionService;
import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import br.gov.pa.prodepa.ps3.service.Ps3App;
import br.gov.pa.prodepa.ps3.service.domain.Bucket;
import br.gov.pa.prodepa.ps3.service.repository.BucketRepository;
import br.gov.pa.prodepa.ps3.service.repository.search.BucketSearchRepository;
import br.gov.pa.prodepa.ps3.service.service.BucketService;
import br.gov.pa.prodepa.ps3.service.service.mapper.BucketMapper;
import br.gov.pa.prodepa.ps3.service.web.rest.BucketResource;
import br.gov.pa.prodepa.ps3.service.web.rest.errors.ExceptionTranslator;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Ps3App.class)
public class ClientStoreResourceTest {

	
	@Autowired
    private BucketRepository bucketRepository;

    @Autowired
    private BucketMapper bucketMapper;

    @Autowired
    private BucketService bucketService;

    @Autowired
    private BucketSearchRepository bucketSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restBucketMockMvc;

    private Bucket bucket;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BucketResource bucketResource = new BucketResource(bucketService);
        this.restBucketMockMvc = MockMvcBuilders.standaloneSetup(bucketResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        //bucketRepository.deleteAll();
        //bucketSearchRepository.deleteAll();
        //bucket = createEntity();
    }

	
	
	@Test
	public void testClientStoreResource() {
		fail("Not yet implemented");
	}

	@Test
	public void testCreateBucket() {
		fail("Not yet implemented");
	}

	@Test
	public void testUpdateBucket() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetAllBuckets() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetBucket() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteBucket() {
		fail("Not yet implemented");
	}

	@Test
	public void testSearchBuckets() {
		fail("Not yet implemented");
	}

	@Test
	public void testPing() throws Exception {

        // Make a PING
        restBucketMockMvc.perform(get("/api/ping/client-store"))       
            .andExpect(status().isOk());
            //.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            
            //.andExpect(jsonPath("$.[*].id").value(hasItem(bucket.getId())))
	}
	

}
